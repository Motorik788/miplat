﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(SaveManager))]
public class SaveObjectCreator : MonoBehaviour, ISave
{
    public bool IsNeedScanResources = true;
    public string IdCreator = "creatorObjs";
    public List<SaveObject> Prefabs = new List<SaveObject>();

    public List<SaveObject> SceneObjects = new List<SaveObject>();

    public static SaveObjectCreator Instance
    {
        get
        {
            if (SaveManager.Instance)
                return SaveManager.Instance.ObjCreator;
            else return null;
        }
    }

    void Awake()
    {
        //print(IsNeedScanResources);
        if (IsNeedScanResources)
        {
            var res = Resources.FindObjectsOfTypeAll(typeof(SaveObject));
            for (int i = 0; i < res.Length; i++)
                Prefabs.Add((res[i] as SaveObject));
        }
    }

    public string GetId()
    {
        return IdCreator;
    }

    bool ISave.IsGlobal()
    {
        return false;
    }

    public void Load(Save save)
    {
        //удаление обьектов созданных после загрузкии не вошедших в текущие сохранение
        var e = SceneObjects.Except(SceneObjects.Where(x => save.Components.Find(y => (string)y.sps[0].Val == x.SaveId) != null));
        for (int i = 0; i < e.Count();)
            Destroy(e.ElementAt(i).gameObject);

        foreach (var item in save.Components)
        {
            var sId = (string)item.sps[0].Val;
            //создание обьекта только, если его нет на сцене
            if (SceneObjects.Find(x => x.SaveId == sId) == null)
            {
                var ob = Prefabs.FirstOrDefault(x => x != null && x.Name == item.Component);
                if (ob)
                {
                    var o = Instantiate(ob);
                    o.SaveId = sId;
                    o.GetComponent<ComponScan>().SaveId = o.SaveId;
                }
            }
        }
    }

    public Save Save()
    {
        Save s = new Save();
        s.Components = new List<SComp>();
        foreach (var item in SceneObjects)
        {
            s.Components.Add(new SComp() { Component = item.Name, sps = new List<SProp>() { new SProp() { Val = item.SaveId } } });
        }

        return s;
    }


    void Start()
    {
        UnityEngine.SceneManagement.SceneManager.sceneLoaded += SceneManager_sceneLoaded;
    }

    private void SceneManager_sceneLoaded(UnityEngine.SceneManagement.Scene arg0, UnityEngine.SceneManagement.LoadSceneMode arg1)
    {
        SceneObjects.Clear();
    }  

    //// Update is called once per frame
    //void Update()
    //{

    //}
}
