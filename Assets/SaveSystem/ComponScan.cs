﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using FullSerializer;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;

public static class CollectionExt
{
    public static object CloneCollection(this System.Collections.IList collection)
    {
        var nColl = System.Activator.CreateInstance(collection.GetType()) as IList;
        foreach (var item in collection)
        {
            nColl.Add(item);
        }

        return nColl;
    }
}



public interface ISave
{
    Save Save();

    void Load(Save save);
    /// <summary>
    /// Must be unique in scene
    /// </summary>
    /// <returns></returns>
    string GetId();

    bool IsGlobal();
}

[System.Serializable]
public class SComp
{
    public string Component;
    public SComp Inner;
    public List<SProp> sps = new List<SProp>();
}

[System.Serializable]
public class SProp
{
    public string Prop;
    public object Val;
}

[System.Serializable]
public class Save
{
    public List<SComp> Components = new List<SComp>();
}


[System.Serializable]
public class Dic : Dictionary<string, EntryDict>, ISerializationCallbackReceiver
{
    List<Entry> entries = new List<Entry>();

    [HideInInspector] public byte[] save;
    /// <summary>
    /// так как сериалайзер юнити с большими ограничениями, пакую словарь в список -> бинарно сериализую ->
    /// пакую в массив байт (с этим юнька справляется) и обратно, только так удалось нормально сохранять структуру сейва
    /// </summary>

    public void OnAfterDeserialize()
    {
        Clear();
        var bf = new BinaryFormatter();
        if (save != null)
        {
            using (var mem = new System.IO.MemoryStream(save))
            {
                entries = bf.Deserialize(mem) as List<Entry>;
                save = null;
            }
        }
        for (int i = 0; i < entries.Count; i++)
        {
            Add(entries[i].Key, entries[i].EntryDict);
        }
        // entries.Clear();
    }

    public void OnBeforeSerialize()
    {
        entries.Clear();
        foreach (var item in this)
        {
            entries.Add(new Entry() { Key = item.Key, EntryDict = item.Value });
        }
        var bf = new BinaryFormatter();
        using (var mem = new System.IO.MemoryStream())
        {
            bf.Serialize(mem, entries);
            save = mem.ToArray();
        }
    }
}

[System.Serializable]
public class Entry
{
    public string Key;
    public EntryDict EntryDict;
    // public List<string> Values;
}

[System.Serializable]
public class EntryDict
{
    public string Name;
    public List<string> Values = new List<string>();
    public EntryDict Inner;
}


public class ComponScan : MonoBehaviour, ISave
{
    [HideInInspector]
    public Dic keyValuePairs = new Dic();

    public bool IsGlobal;

    public string SaveId = System.Guid.NewGuid().ToString();

    public string GetId()
    {
        return SaveId;
    }

    bool ISave.IsGlobal()
    {
        return this.IsGlobal;
    }

    public void OnDestroy()
    {
        SaveManager.UnRegister(this);
    }

    public void Load(Save save)
    {
        foreach (var item in save.Components)
        {
            object obj = null;
            //Случай для геймобжекта, тк это не компонент
            if (item.Component == "GameObject")
                obj = gameObject;
            else
                obj = GetComponent(item.Component);
            var type = obj.GetType();
            LoadRecursive(item, type, obj);
        }
    }

    void LoadRecursive(SComp comp, System.Type type, object item)
    {
        if (comp.Inner != null)
        {
            var f = type.GetField(comp.Inner.Component);
            LoadRecursive(comp.Inner, f.FieldType, f.GetValue(item));
        }

        foreach (var pf in comp.sps)
        {
            try
            {
                if (type.GetField(pf.Prop) != null)
                {
                    var field = pf.Val;
                    object ob = null;

                    var tryArray = field as System.Array;
                    if (tryArray != null)
                        ob = tryArray.Clone();
                    else if (typeof(IList).IsAssignableFrom(field.GetType()))
                    {
                        var col = field as IList;
                        ob = col.CloneCollection();
                    }

                    ob = ob == null ? pf.Val : ob;

                    type.GetField(pf.Prop).SetValue(item, ob);
                }
                else
                    type.GetProperty(pf.Prop).SetValue(item, pf.Val, null);
            }
            catch (System.Exception ex)
            {
                print(ex.Message);
            }
        }
    }

    public Save Save()
    {
        var save = new Save();
        foreach (var item in keyValuePairs)
        {
            var s = new SComp();
            try
            {
                object obj = null;
                //Случай для геймобжекта, тк это не компонент
                if (item.Key == "GameObject")
                    obj = gameObject;
                else
                    obj = GetComponent(item.Key);
                var type = obj.GetType();
                s.Component = type.Name;
                SaveRecursive(obj, s, item.Value);
                save.Components.Add(s);
            }
            catch (System.Exception ex)
            {
                print(ex);
                //throw;
            }

        }
        return save;
    }

    void SaveRecursive(object item, SComp comp, EntryDict entry)
    {
        var type = item.GetType();

        //я не поинмаю откуда берется лишний Inner, но второй проверкой можно его выявить
        if (entry.Inner != null && !string.IsNullOrEmpty(entry.Inner.Name))
        {
            comp.Inner = new SComp();
            comp.Inner.Component = entry.Inner.Name;
            SaveRecursive(type.GetField(entry.Inner.Name).GetValue(item), comp.Inner, entry.Inner);
        }

        foreach (var p in entry.Values)
        {
            object v = type.GetField(p) != null ? type.GetField(p).GetValue(item) : type.GetProperty(p).GetValue(item, null);
            comp.sps.Add(new SProp() { Prop = p, Val = v });
        }
    }

    public void Awake()
    {
        SaveManager.Register(this);
    }
}
