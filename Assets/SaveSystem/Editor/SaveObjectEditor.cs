﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SaveObject))]
public class SaveObjectEditor : Editor
{
    SaveObject obj;
    void RandID()
    {
        if (!obj)
            obj = target as SaveObject;
        obj.SaveId = System.Guid.NewGuid().ToString();
        obj.GetComponent<ComponScan>().SaveId = obj.SaveId;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (!obj)
            obj = target as SaveObject;

        if (!obj.IsNeedRandomIdOnStart && string.IsNullOrEmpty(obj.SaveId))
            RandID();
        if (!Application.isPlaying &&  obj.IsNeedRandomIdOnStart && !string.IsNullOrEmpty(obj.SaveId))
            obj.SaveId = "";
        if (GUILayout.Button("Random ID"))
        {
            RandID();
        }
    }
}
