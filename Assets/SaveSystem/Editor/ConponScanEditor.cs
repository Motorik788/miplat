﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

[CustomEditor(typeof(ComponScan))]
public class ConponScanEditor : Editor
{

    Dictionary<string, bool> d = new Dictionary<string, bool>();

    public override void OnInspectorGUI()
    {

        base.OnInspectorGUI();
        serializedObject.Update();
        var cmp = (target as MonoBehaviour).GetComponents<Component>();
        var scan = target as ComponScan;

        OnGUIComponentShow(scan.gameObject, scan.gameObject.GetType(), scan);
        foreach (var item in cmp)
        {
            var type = item.GetType();
            OnGUIComponentShow(item, type, scan);
        }

        serializedObject.ApplyModifiedProperties();
    }

    bool Exclude(object item)
    {
        System.Type[] excl =
        {
            typeof(AudioListener),
            typeof(ComponScan),
            typeof(MeshFilter),
           // typeof(MeshRenderer),
            typeof(SaveObject)
        };

        bool res = false;
        var t = item.GetType();

        foreach (var i in excl)
        {
            res |= t.IsAssignableFrom(i);
        }
        return !res;
    }

    void OnGUIComponentShow(object item, System.Type type, ComponScan scan, string propName = null, string propFullName = null, string propText = null, EntryDict entry = null, bool needEnableTogle = true)
    {
        if (Exclude(item))
        {
            propName = !string.IsNullOrEmpty(propName) ? propName : type.Name;
            propFullName = !string.IsNullOrEmpty(propFullName) ? propFullName : propName;
            propText = !string.IsNullOrEmpty(propText) ? propText : propFullName;
            if (!d.ContainsKey(propFullName))
                d[propFullName] = false;
            var fields = type.GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
            var props = type.GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
            //var enabledProp = type.GetProperty("enabled");
            //if (enabledProp != null)
            //{
            //    var propsWithEnabled = new System.Reflection.PropertyInfo[props.Length + 1];
            //    props.CopyTo(propsWithEnabled, 1);
            //    propsWithEnabled[0] = enabledProp;
            //    props = propsWithEnabled;
            //}

            if (fields.Length > 0 || props.Length > 0)
            {
                d[propFullName] = EditorGUILayout.Foldout(d[propFullName], propText);
                if (d[propFullName])
                {
                    var en = needEnableTogle ? EditorGUILayout.BeginToggleGroup("Enable", scan.keyValuePairs.ContainsKey(propName)) : true;
                    if (en != scan.keyValuePairs.ContainsKey(propName))
                    {
                        Undo.RecordObject(target, "Component enable save");
                        EditorUtility.SetDirty(target);
                    }
                    if (en)
                    {
                        if (!scan.keyValuePairs.ContainsKey(propName))
                            scan.keyValuePairs[propName] = new EntryDict() { Name = propName };
                        entry = entry == null ? scan.keyValuePairs[propName] : entry;
                        //Debug.Log(entry.Name);
                    }
                    else
                    {
                        if (scan.keyValuePairs.ContainsKey(propName))
                            scan.keyValuePairs.Remove(propName);
                    }

                    foreach (var f in fields)
                    {
                        try
                        {
                            if (ExcludeMaterialType(f.FieldType))
                                continue;

                            if (type != typeof(GameObject) && DeclaringInUnityObject(f.DeclaringType))
                                continue;
                            var t = f.FieldType;

                            if (!TypeIsCollectionOrArray(t) && !t.IsPrimitive && t.GetCustomAttributes(typeof(System.SerializableAttribute), false).GetLength(0) > 0 && t != typeof(string))
                            {
                                entry = entry == null ? new EntryDict() : entry;

                                if (entry.Inner == null || entry.Inner.Name == null)
                                {
                                    entry.Inner = new EntryDict();
                                    entry.Inner.Name = f.Name;
                                }

                                OnGUIComponentShow(f.GetValue(item), t, scan, propName, propFullName + "." + f.Name, f.Name, entry.Inner, false);
                                continue;
                            }
                            var val = f.GetValue(item);
                            if (val != null && !(val is UnityEngine.Object))
                            {
                                if (!en)
                                {
                                    EditorGUILayout.LabelField(f.Name, val.ToString());
                                }
                                else
                                {
                                    var cont = entry.Values.Contains(f.Name);
                                    var en2 = EditorGUILayout.Toggle(f.Name, cont);
                                    if (en2 != cont)
                                    {
                                        Undo.RecordObject(target, "component field marked");
                                       
                                        EditorUtility.SetDirty(target);
                                    }

                                    if (en2 && !cont)
                                        entry.Values.Add(f.Name);
                                    else if (cont && !en2)
                                        entry.Values.Remove(f.Name);
                                }
                            }
                        }
                        catch (System.Exception ex)
                        {
                            Debug.Log(ex);

                        }
                    }

                    foreach (var p in props)
                    {
                        if (ExcludeMaterialType(p.PropertyType))
                            continue;

                        if (type != typeof(GameObject) && DeclaringInUnityObject(p.DeclaringType) && !(p.DeclaringType == typeof(Behaviour) && p.Name == "enabled"))
                            continue;
                        if (p.CanRead && p.CanWrite && p.GetCustomAttributes(typeof(System.ObsoleteAttribute), false).Length == 0)
                        {
                            try
                            {
                                // Debug.Log(p.ToString());
                                var val = p.GetValue(item, null);
                                if (val != null && !(val is UnityEngine.Object))
                                {
                                    if (!en)
                                    {
                                        EditorGUILayout.LabelField(p.Name, val.ToString());
                                    }
                                    else
                                    {
                                        var cont = scan.keyValuePairs[propName].Values.Contains(p.Name);
                                        var en2 = EditorGUILayout.Toggle(p.Name, scan.keyValuePairs[type.Name].Values.Contains(p.Name));

                                        if (en2 != cont)
                                        {
                                            Undo.RecordObject(target, "component property marked");
                                            EditorUtility.SetDirty(target);
                                        }

                                        if (en2 && !cont)
                                            scan.keyValuePairs[propName].Values.Add(p.Name);
                                        else if (cont && !en2)
                                            scan.keyValuePairs[propName].Values.Remove(p.Name);
                                    }
                                }
                            }
                            catch (System.Exception)
                            {
                               

                            }
                        }
                    }
                    if (needEnableTogle)
                        EditorGUILayout.EndToggleGroup();
                    if (d[propFullName])
                        EditorGUILayout.Space();
                }
            }
        }
    }


    private bool DeclaringInUnityObject(System.Type type)
    {
        return (type == typeof(MonoBehaviour)
            || type == typeof(Behaviour)
            || type == typeof(Component)
            || type == typeof(Object));
    }

    private bool ExcludeMaterialType(System.Type type)
    {
        return type == typeof(Material) || type == typeof(Material[]);
    }

    private bool TypeIsCollectionOrArray(System.Type type)
    {
        return type.IsSubclassOf(typeof(System.Array)) || typeof(System.Collections.IList).IsAssignableFrom(type);
    }
}
