﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ComponScan))]
public class SaveObject : MonoBehaviour
{
    public string Name;
    public bool IsNeedRandomIdOnStart = true;
    [HideInInspector]
    public string SaveId;

    private void Start()
    {
        if (string.IsNullOrEmpty(Name))
            Name = gameObject.name;
        SaveObjectCreator.Instance.SceneObjects.Add(this);
        if (IsNeedRandomIdOnStart && string.IsNullOrEmpty(SaveId))
        {
            SaveId = System.Guid.NewGuid().ToString();
            GetComponent<ComponScan>().SaveId = SaveId;
        }
    }


    private void OnDisable()
    {
        if (SaveObjectCreator.Instance)
            SaveObjectCreator.Instance.SceneObjects.Remove(this);
    }

    //public void OnDestroy()
    //{
    //    OnDisable();
    //}

    //// Use this for initialization
    //void Start()
    //{

    //}

    //// Update is called once per frame
    //void Update()
    //{

    //}
}
