﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullSerializer;
using UnityEngine.SceneManagement;
using System.Linq;

public class SaveManager : MonoBehaviour
{

    public class Cm : EqualityComparer<ISave>
    {
        public override bool Equals(ISave x, ISave y)
        {
            return x.GetId() == y.GetId();
        }

        public override int GetHashCode(ISave obj)
        {
            return obj.GetId().GetHashCode();
        }
    }
    public enum SaveType
    {
        PlayerPrefs,
        ToFile
    }

    public static SaveManager Instance
    {
        get
        {
            if (applicationIsQuitting)
                return null;
            if (instance != null)
                return instance;

            instance = FindObjectOfType<SaveManager>();

            if (instance != null)
                return instance;

            Create();
            return instance;
        }
    }

    public event System.Action Saved;
    public event System.Action Loaded;

    private static SaveManager instance;

#if UNITY_EDITOR

    [UnityEditor.MenuItem("SaveManager/Create")]
    public static void CreateSaveManager()
    {
        Create();
    }
#endif

    static void Create()
    {
        var go = new GameObject("SaveManager");
        // DontDestroyOnLoad(go);
        instance = go.AddComponent<SaveManager>();
        go.AddComponent<SaveObjectCreator>();
    }

    public SaveType Type;
    [Header("Application.persistentDataPath/Application.productName/FileName.sav")]
    public string FileName;

    //public Dictionary<string, Save> Data = new Dictionary<string, Save>();
    public HashSet<ISave> SaveObjects = new HashSet<ISave>(new Cm());
    public Dictionary<string, Dictionary<string, Save>> scenesData = new Dictionary<string, Dictionary<string, Save>>();

    [HideInInspector]
    public SaveObjectCreator ObjCreator;

    private void Awake()
    {
        if (Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(this.gameObject);
        ObjCreator = GetComponent<SaveObjectCreator>();
        scenesData["global"] = new Dictionary<string, Save>();
        if (Type == SaveType.ToFile)
            if (!string.IsNullOrEmpty(FileName))
                FileName = Application.persistentDataPath + "/" + Application.productName + "/" + FileName + ".sav";
            else
                Debug.LogError("File name is empty!!");
        UnityEngine.SceneManagement.SceneManager.sceneLoaded += SceneManager_sceneLoaded;

    }

    private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        List<ISave> markAsUnRegister = new List<ISave>();
        foreach (var item in SaveObjects)
        {
            var i = item as MonoBehaviour;
            if (i == null)
                markAsUnRegister.Add(item);
        }
        foreach (var item in markAsUnRegister)
        {
            UnRegister(item);
        }
    }

    private static bool applicationIsQuitting = false;

    public void OnDestroy()
    {
        //Debug.Log("Gets destroyed");
        if (instance == this)
            applicationIsQuitting = true;
    }


    public static void Register(ISave saveOb)
    {
        if (!Instance.SaveObjects.Contains(saveOb))
            Instance.SaveObjects.Add(saveOb);
    }

    public static void UnRegister(ISave saveOb)
    {
        if (Instance && Instance.SaveObjects.Contains(saveOb))
            Instance.SaveObjects.Remove(saveOb);
    }

    /// <summary>
    /// Удаляет из сейва данные конкретной сохраненной сцены, если не передавать имя, будет удален сейв текущей сцены
    /// </summary>
    /// <param name="sceneName"></param>
    public void DeleteSceneSave(string sceneName = null)
    {
        if (string.IsNullOrEmpty(sceneName))
            sceneName = SceneManager.GetActiveScene().name;
        if (scenesData.ContainsKey(sceneName))
            scenesData.Remove(sceneName);
    }

    public void Save()
    {
        StopAllCoroutines();
        var scene = SceneManager.GetActiveScene().name;
        Dictionary<string, Save> Data = scenesData.ContainsKey(scene) ? scenesData[scene] : null;
        var dataGlobal = scenesData["global"];
        if (Data == null)
        {
            Data = new Dictionary<string, Save>();
            scenesData[SceneManager.GetActiveScene().name] = Data;
        }
        foreach (var item in SaveObjects)
        {
            var id = item.GetId();
            if (!string.IsNullOrEmpty(id))
            {
                if (!item.IsGlobal())
                    Data[id] = item.Save();
                else
                    dataGlobal[id] = item.Save();

            }
        }
        if (ObjCreator && ObjCreator.SceneObjects.Count > 0)
        {
            Data[ObjCreator.GetId()] = ObjCreator.Save();
        }
        //print("Saved! ok");
        StartCoroutine(saveCoroutine());
    }


    bool ok = false;
    IEnumerator saveCoroutine()
    {
        fsData fsData = null;
        ok = false;
        string json = null;
        System.Threading.ThreadPool.QueueUserWorkItem(x =>
        {
            fsSerializer f = new fsSerializer();
            // print(scenesData.Count);
            f.TrySerialize<Dictionary<string, Dictionary<string, Save>>>(scenesData, out fsData).AssertSuccessWithoutWarnings();
            json = fsJsonPrinter.CompressedJson(fsData);

            if (Type == SaveType.ToFile)
            {
                System.IO.File.WriteAllText(FileName, json);
                json = null;
            }
            System.GC.Collect();
            ok = true;
        });
        yield return new WaitWhile(() => !ok);
        if (Type == SaveType.PlayerPrefs)
        {
            //print(fsJsonPrinter.PrettyJson(fsData));
            PlayerPrefs.SetString("save", json);
            PlayerPrefs.Save();
        }

        // print("Saved! end");
        if (Saved != null)
            Saved();
    }


    public void Load()
    {
        StopAllCoroutines();
        ok = false;
        fsSerializer f = new fsSerializer();
        Dictionary<string, Dictionary<string, Save>> s = null;
        fsData data = null;
        // чтобы не загружать с диска, можно пользоваться сохранением в памяти
        if (scenesData.Count == 1)
        {
            if (Type == SaveType.PlayerPrefs)
            {
                if (PlayerPrefs.HasKey("save"))
                    data = fsJsonParser.Parse(PlayerPrefs.GetString("save"));
                //if (data != null)
                //print(fsJsonPrinter.PrettyJson(data));
                //System.IO.File.WriteAllText(System.Environment.CurrentDirectory + "/t.json", fsJsonPrinter.PrettyJson(data));
            }
            //print("read started");
            System.Threading.ThreadPool.QueueUserWorkItem(x =>
            {
                if (Type == SaveType.ToFile)
                    if (System.IO.File.Exists(FileName))
                        data = fsJsonParser.Parse(System.IO.File.ReadAllText(FileName));
                if (data != null)
                {
                    f.TryDeserialize<Dictionary<string, Dictionary<string, Save>>>(data, ref s).AssertSuccessWithoutWarnings();
                    scenesData = s;
                    data = null;
                }
                System.GC.Collect();
                ok = true;
            });
        }
        else
            ok = true;
        StartCoroutine(loadCoroutine());
    }

    IEnumerator loadCoroutine()
    {
        yield return new WaitWhile(() => !ok);
        var Data = scenesData.ContainsKey(SceneManager.GetActiveScene().name) ? scenesData[SceneManager.GetActiveScene().name] : null;
        var dataGlobal = scenesData["global"];

        if (Data != null && ObjCreator && ObjCreator.SceneObjects.Count > 0)
            ObjCreator.Load(Data[ObjCreator.GetId()]);
        foreach (var item in SaveObjects)
        {
            var id = item.GetId();
            if (!item.IsGlobal())
            {
                if (Data != null && Data.ContainsKey(id))
                {
                    item.Load(Data[id]);
                }
            }
            else
            {
                if (dataGlobal.ContainsKey(id))
                {
                    item.Load(dataGlobal[id]);
                }
            }
        }
        if (Loaded != null)
            Loaded();
        //print("Save loaded! ");
    }

    public void ClearSave()
    {
        if (Type == SaveType.PlayerPrefs && PlayerPrefs.HasKey("save"))
            PlayerPrefs.DeleteKey("save");
        else
        {
            if (System.IO.File.Exists(FileName))
                System.IO.File.Delete(FileName);
        }

        scenesData.Clear();
        scenesData["global"] = new Dictionary<string, Save>();
    }

    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.S))
        //{
        //    SaveManager.Instance.Save();
        //}
        //if (Input.GetKeyDown(KeyCode.L))
        //{
        //    SaveManager.Instance.Load();
        //}
        //if (Input.GetKeyDown(KeyCode.C))
        //{
        //    ClearSave();
        //}
    }
}
