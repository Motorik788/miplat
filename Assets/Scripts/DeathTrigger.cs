﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathTrigger : TriggerAction2D
{
    public void KillUnit(Unit unit)
    {
        unit.Health = 0;      
    }
}
