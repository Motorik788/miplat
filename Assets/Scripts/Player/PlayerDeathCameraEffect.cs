﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class PlayerDeathCameraEffect : MonoBehaviour
{
    public AudioClip audioEffect;
    public PostProcessingProfile Effectprofile;
    public Cinemachine.CinemachineVirtualCamera TargetCamera;
    public float TimeScaleValue = 1;
    public float SpeedCameraMoving = 1;

    private float OrtoSize;

    void Start()
    {
        if (TargetCamera)
            OrtoSize = TargetCamera.m_Lens.OrthographicSize;
    }


    void Update()
    {
        if (isStarted)
        {
            TargetCamera.m_Lens.OrthographicSize = Mathf.MoveTowards(TargetCamera.m_Lens.OrthographicSize, OrtoSize / 2, Time.deltaTime * SpeedCameraMoving);
            if (Input.anyKeyDown)
            {
                StopAllCoroutines();
                Time.timeScale = 1;
                GameManager.Instance.LoadLastSave();
            }
        }
    }


    bool isStarted = false;
    public void StartEffect()
    {
        if (audioEffect && TargetCamera)
            StartCoroutine(EffectCoroutine());
    }

    IEnumerator EffectCoroutine()
    {
        isStarted = true;
        GameSettingsSingleton.Instance.SoundSettings.PlayMusic(audioEffect);
        Time.timeScale = TimeScaleValue;
        var CameraPostProcessing = Camera.main.GetComponent<PostProcessingBehaviour>();
        if (CameraPostProcessing)
            CameraPostProcessing.profile = Effectprofile;
        yield return new WaitForSecondsRealtime(audioEffect.length - 5);
        Time.timeScale = 1;
        GameManager.Instance.LoadLastSave();
    }
}
