﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEditor;
using System;

public class EnumMaskPropertyAttribute : PropertyAttribute { }

#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(EnumMaskPropertyAttribute))]
public class EnumMaskProperty : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var type = fieldInfo.FieldType;
        var enumVal = EditorGUI.EnumFlagsField(position, label, (Enum)System.Enum.ToObject(type, property.intValue));
        property.intValue = (int)Convert.ChangeType(enumVal, typeof(int));
    }
}



#endif

public class HitInfo
{
    //[System.Flags]
    public enum DamageType
    {
        Hit = 1,
        Byte = 2,
        Explosion = 4,
        Area = 8,
        Projectile = 16
    }

    public Unit From;
    public float Value;
    public float MaxValue = -1;
    public DamageType TypeDamage = DamageType.Hit;
}

public static class ApplyDamageHelper
{
    public static void ApplyDamageClose(Unit from)
    {
        var hit = Physics2D.Raycast(from.transform.position, from.transform.right, from.WeaponDistance, from.LayerMaskAttak);
        if (hit.transform)
        {
            var to = hit.transform.GetComponent<Unit>();
            if (to)
            {
                to.Damage(new HitInfo() { From = from, Value = from.WeaponDamage });
            }
        }
    }
}

public interface IApplyDamageble
{
    void ApplyDamage();
}

public class Unit : MonoBehaviour
{
    [System.Serializable]
    public class DeathEvent : UnityEvent { }
    [System.Serializable]
    public class DamageEvent : UnityEvent<HitInfo> { }

    [Serializable]
    public class ImmunityDamageLayer
    {
        public HitInfo.DamageType DamageType;
        [Range(0, 1)]
        public float Value;
    }


    public float MaxHealth;
    public float Armor;
    public float MaxArmor = 100;
    public float WeaponDamage;
    public float WeaponDistance;
    public float WeaponForce;
    public bool Immortal;
    public bool IsDeath;
    [EnumMaskProperty]
    public HitInfo.DamageType DamageTypeMask;
    public List<ImmunityDamageLayer> Immunities;
    public LayerMask LayerMaskAttak;
    public DeathEvent OnDeath;
    public DamageEvent OnDamage;

    public float Health
    {
        get
        {
            if (health <= 0 && !Immortal && !IsDeath)
                OnDeath.Invoke();
            return health;
        }
        set
        {
            health = value;
            if (health <= 0 && !Immortal && !IsDeath)
                OnDeath.Invoke();
        }
    }

    [HideInInspector]
    public Animator anim;
    [HideInInspector]
    public Rigidbody2D rigid;
    private float health;


    protected void Start()
    {
        health = MaxHealth;
        anim = GetComponent<Animator>();
        rigid = GetComponent<Rigidbody2D>();
    }


    //protected void Update()
    //{

    //}

    public virtual void Death()
    {
        IsDeath = true;
    }

    /// <summary>
    /// Возвращает true, если условие для получения урона тру. Возвращаемое значение для переопределяющих
    /// </summary>
    /// <param name="info"></param>
    /// <returns></returns>
    public virtual bool Damage(HitInfo info)
    {
        if (!IsDeath && (DamageTypeMask & info.TypeDamage) != 0)
        {
            OnDamage.Invoke(info);

            var val = info.Value;
            if (Armor > 0 && DamageForArmor(info.TypeDamage))
            {
                if (Armor > info.Value)
                {
                    Armor -= info.Value;
                    val = 0;
                }
                else
                {
                    val -= Armor;
                    Armor = 0;
                }
            }
            var hitValue = val * (1 - GetImmunity(info.TypeDamage));
            Health -= hitValue;
            return true;
        }
        return false;
    }

    private bool DamageForArmor(HitInfo.DamageType damageType)
    {
        return damageType == HitInfo.DamageType.Byte || damageType == HitInfo.DamageType.Explosion
            || damageType == HitInfo.DamageType.Hit || damageType == HitInfo.DamageType.Projectile;
    }

    public float GetImmunity(HitInfo.DamageType damageType)
    {
        var immunity = Immunities.Find(x => x.DamageType == damageType);
        return immunity != null ? immunity.Value : 0;
    }

    public void SetArmor(float armor)
    {
        Armor = armor;
        Armor = Armor > MaxArmor ? MaxArmor : Armor;
    }

    public void SetImmunity(HitInfo.DamageType damageType, float val)
    {
        var immunity = Immunities.Find(x => x.DamageType == damageType);
        if (immunity == null)
        {
            immunity = new ImmunityDamageLayer() { DamageType = damageType, Value = 0 };
            Immunities.Add(immunity);
        }
        immunity.Value = val;
        if (immunity.Value > 1)
            immunity.Value = 1;
    }
}
