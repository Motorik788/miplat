﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InventarCell
{
    public ObjectInfo Object;
    public int Count;
}

public class PlayerUnit : Unit, IApplyDamageble, ISave
{
    public enum LookDir
    {
        Right,
        Left
    }

    public enum InventarItemAction
    {
        Add,
        Remove
    }

    public List<InventarCell> InventarList = new List<InventarCell>();
    public TrowableObject CurrentTrowable;
    public event System.Action<InventarCell, InventarItemAction> InventarEvent;

    [HideInInspector]
    public CapsuleCollider2D Coll;
    [HideInInspector]
    public LookDir lookDir = LookDir.Right;
    private SoundEffect soundEffect;
    private BonusEffectSystem effectSystem;

    public event System.Action<TrowableObject> ChangeCurrentThrowable;

    public SoundEffect SoundEffect
    {
        get
        {
            return soundEffect;
        }
    }

    new void Start()
    {
        base.Start();
        Coll = GetComponent<CapsuleCollider2D>();
        SaveManager.Register(this);
        soundEffect = GetComponent<SoundEffect>();
        effectSystem = GetComponent<BonusEffectSystem>();
    }

    public void OnDestroy()
    {
        SaveManager.UnRegister(this);
    }

    public void AddItem(ObjectInfo item, int count = 1)
    {
        var cell = InventarList.Find(x => x.Object == item);
        if (cell == null)
        {
            cell = new InventarCell() { Object = item, Count = count };
            InventarList.Add(cell);
        }
        else
            cell.Count += count;
        if (InventarEvent != null)
            InventarEvent(cell, InventarItemAction.Add);
    }

    public void RemoveItem(ObjectInfo item)
    {
        var cell = InventarList.Find(x => x.Object == item);
        if (cell != null)
        {
            cell.Count--;
            if (cell.Count == 0)
            {
                InventarList.Remove(cell);
                if (item == CurrentTrowable)
                    SetThrowable(null);
            }
            if (InventarEvent != null)
                InventarEvent(cell, InventarItemAction.Remove);
        }
    }

    public void SetThrowable(TrowableObject trowableObject, bool withoutSound = false)
    {
        CurrentTrowable = trowableObject;
        if (ChangeCurrentThrowable != null)
            ChangeCurrentThrowable(CurrentTrowable);
        if (trowableObject && trowableObject.soundUse && !withoutSound)
        {
            soundEffect.PlaySound(trowableObject.soundUse);
        }
    }

    private float timeLastThrow = 0;

    void Update()
    {

        if (!IsDeath && !GameManager.Instance.IsPaused)
        {
            if (InputManager.Instance.GetActionButton("attak", true))
            {
                //if (soundEffect)
                //    soundEffect.PlaySound("Attak");
                if (!anim.GetBool("Walk"))
                    anim.SetTrigger("Attak");
                else
                    anim.SetTrigger("JumpAttak");
            }
            else if (InputManager.Instance.GetActionButton("throw", true))
            {
                if (CurrentTrowable && Time.time - timeLastThrow >= 0.5f)
                {
                    timeLastThrow = Time.time;
                    soundEffect.PlaySound("Throw");
                    var item = Instantiate(CurrentTrowable, CurrentTrowable.transform.position, Quaternion.identity);
                    item.transform.position = transform.position + transform.right;
                    var rigid = item.GetComponent<Rigidbody2D>();
                    if (rigid)
                    {
                        rigid.AddTorque(-8);
                        rigid.AddRelativeForce((transform.right + transform.up) * 100 * item.ThrowForceMultiply);
                    }
                    item.Use();
                    item.SetFromUnit(this);
                    RemoveItem(CurrentTrowable);
                }
            }
        }
    }

    public void ApplyDamage()
    {
        ApplyDamageHelper.ApplyDamageClose(this);
    }

    public override void Death()
    {
        base.Death();
        anim.SetBool("Walk", false);
        anim.SetBool("Run", false);
        Coll.size = new Vector2(Coll.size.x, Coll.size.y - 1);

        anim.SetTrigger("Dead");
        GameManager.Instance.PlayerDeathInc();
    }

    public override bool Damage(HitInfo info)
    {
        var res = base.Damage(info);
        if (!IsDeath && info.TypeDamage == HitInfo.DamageType.Explosion && info.MaxValue != -1 && info.Value / info.MaxValue > 0.5f)
        {
            soundEffect.PlaySound("Bomb");
            Bonus b = new Bonus()
            {
                IsTemp = true,
                Time = 9,
                Value = -1.5f,
                Name = "kek",
                Type = Bonus.EffectType.ChangeSpeed
            };
            var ef = effectSystem.ActiveBonuses.Find(x => x.Name == b.Name);
            if (ef != null)
                effectSystem.RemoveBonus(ef);
            effectSystem.AddBonus(b);
        }

        return res;
    }

    public Save Save()
    {
        Save s = new Save();
        SComp comp = new SComp();
        comp.Component = "Inventar";
        SProp sProp = new SProp();
        List<KeyValuePair<int, int>> inventar = new List<KeyValuePair<int, int>>();
        foreach (var item in InventarList)
        {
            var id = ItemsRegistry.Instance.GetIndexByObject(item.Object);
            inventar.Add(new KeyValuePair<int, int>(id, item.Count));
        }
        sProp.Prop = "list";
        sProp.Val = inventar;
        comp.sps.Add(sProp);


        sProp = new SProp();
        sProp.Prop = "CurTrow";
        if (CurrentTrowable)
            sProp.Val = ItemsRegistry.Instance.GetIndexByObject(CurrentTrowable);
        else
            sProp.Val = null;
        comp.sps.Add(sProp);


        s.Components.Add(comp);
        return s;
    }

    public void Load(Save save)
    {
        var prop = save.Components[0].sps[0];
        InventarList.Clear();
        foreach (var item in (List<KeyValuePair<int, int>>)prop.Val)
        {
            var obj = ItemsRegistry.Instance.GetObjectByIndex(item.Key);
            AddItem(obj, item.Value);
        }
        prop = save.Components[0].sps[1];
        if (prop.Val != null)
        {
            var objTrow = ItemsRegistry.Instance.GetObjectByIndex((int)prop.Val);
            SetThrowable(objTrow as TrowableObject, true);
        }
    }

    public string GetId()
    {
        return "PlayerInventar";
    }

    public bool IsGlobal()
    {
        return true;
    }
}
