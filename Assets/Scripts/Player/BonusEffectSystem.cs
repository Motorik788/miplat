﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusEffectSystem : MonoBehaviour
{
    public List<Bonus> ActiveBonuses = new List<Bonus>();
    public float IncrementTime;

    public static BonusEffectSystem Instance
    {
        get
        {
            return instance;
        }
    }

    private static BonusEffectSystem instance;

    private PlayerMove PlayerMove;
    private PlayerUnit PlayerUnit;

    private void Awake()
    {
        if (instance == null && instance != this)
            instance = this;
        else
            Destroy(this);
    }

    void Start()
    {
        PlayerUnit = GetComponent<PlayerUnit>();
        PlayerMove = GetComponent<PlayerMove>();
    }


    void Update()
    {
        for (int i = 0; i < ActiveBonuses.Count; i++)
        {
            var item = ActiveBonuses[i];
            if (item.IsTemp)
            {
                item.Time -= Time.deltaTime;
                if (item.IsIncrement && Time.time - item.IncrementLastTime >= IncrementTime)
                {
                    ApplyBonus(item);
                    item.IncrementLastTime = Time.time;
                }
                if (item.Time <= 0)
                {
                    if (!item.IsIncrement)
                        RollbackBonus(item);
                    ActiveBonuses.RemoveAt(i);
                }
            }
        }
    }

    public void AddBonus(Bonus bonus)
    {
        ApplyBonus(bonus);
        if (bonus.IsTemp)
        {
            var immunBonus = bonus as ImmunityBonus;
            if (immunBonus != null)
                bonus = new ImmunityBonus(immunBonus);
            else
                bonus = new Bonus(bonus);
            ActiveBonuses.Add(bonus);
        }
    }

    public void RemoveBonus(Bonus bonus)
    {
        RollbackBonus(bonus);
        ActiveBonuses.Remove(bonus);
    }

    public void ApplyBonus(Bonus bonus)
    {
        switch (bonus.Type)
        {
            case Bonus.EffectType.ChangeMaxHealth:
                PlayerUnit.MaxHealth += bonus.Value;
                PlayerUnit.Health = PlayerUnit.MaxHealth;
                break;
            case Bonus.EffectType.ChangeHealth:
                PlayerUnit.Health += bonus.Value;
                if (PlayerUnit.Health > PlayerUnit.MaxHealth)
                    PlayerUnit.Health = PlayerUnit.MaxHealth;
                break;
            case Bonus.EffectType.ChangeArmor:
                var arm = PlayerUnit.Armor;
                arm += bonus.Value;
                PlayerUnit.SetArmor(arm);
                break;
            case Bonus.EffectType.ChangeMaxArmor:
                PlayerUnit.MaxArmor += bonus.Value;
                break;
            case Bonus.EffectType.ChangeWeapon:
                PlayerUnit.WeaponDamage += bonus.Value;
                break;
            case Bonus.EffectType.ChangeSpeed:
                PlayerMove.SpeedWalk += bonus.Value;
                PlayerMove.SpeedRun += bonus.Value;
                break;
            case Bonus.EffectType.GetImmunityHit:
                var h = PlayerUnit.GetImmunity(HitInfo.DamageType.Hit);
                bonus.PrevValue = h;
                PlayerUnit.SetImmunity(HitInfo.DamageType.Hit, bonus.Value);
                break;
            case Bonus.EffectType.GetImmunityBonus:
                bonus.Apply(PlayerUnit, PlayerMove);
                break;
            case Bonus.EffectType.GetImmunityArea:
                var h1 = PlayerUnit.GetImmunity(HitInfo.DamageType.Area);
                bonus.PrevValue = h1;
                PlayerUnit.SetImmunity(HitInfo.DamageType.Area, bonus.Value);
                break;
            default:
                break;
        }
    }

    public void RollbackBonus(Bonus bonus)
    {
        switch (bonus.Type)
        {
            case Bonus.EffectType.ChangeMaxHealth:
                PlayerUnit.MaxHealth -= bonus.Value;
                PlayerUnit.Health = PlayerUnit.MaxHealth;
                break;
            case Bonus.EffectType.ChangeArmor:
                var arm = PlayerUnit.Armor;
                arm -= bonus.Value;
                PlayerUnit.SetArmor(arm);
                break;
            case Bonus.EffectType.ChangeMaxArmor:
                PlayerUnit.MaxArmor -= bonus.Value;
                break;
            case Bonus.EffectType.ChangeWeapon:
                PlayerUnit.WeaponDamage -= bonus.Value;
                break;
            case Bonus.EffectType.ChangeSpeed:
                PlayerMove.SpeedWalk -= bonus.Value;
                PlayerMove.SpeedRun -= bonus.Value;
                break;
            case Bonus.EffectType.GetImmunityHit:
                PlayerUnit.SetImmunity(HitInfo.DamageType.Hit, bonus.PrevValue);
                break;
            case Bonus.EffectType.GetImmunityArea:
                PlayerUnit.SetImmunity(HitInfo.DamageType.Area, bonus.PrevValue);
                break;
            case Bonus.EffectType.GetImmunityBonus:
                bonus.RollBack(PlayerUnit, PlayerMove);
                break;
            default:
                break;
        }
    }

}
