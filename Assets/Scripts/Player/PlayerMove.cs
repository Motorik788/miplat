﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMove : MonoBehaviour
{
    public float SpeedWalk;
    public float SpeedRun;
    public float JumpForce;
    public float FootStepInterval = 0.5f;
    public LayerMask LayerMaskRaycastGround;

    private Animator anim;
    private Rigidbody2D rigid;
    private new Collider2D collider2D;
    private PlayerUnit unit;

    private SoundEffect soundEffect;
    private float footStepTime;
    private bool isRun;
    private float footStepIntervalRun;



    void Start()
    {
        anim = GetComponent<Animator>();
        rigid = GetComponent<Rigidbody2D>();
        collider2D = GetComponent<Collider2D>();
        unit = GetComponent<PlayerUnit>();
        soundEffect = GetComponent<SoundEffect>();
        footStepIntervalRun = FootStepInterval / 1.9f;
    }

    private bool onLedder;

    public void SetOnLedder(bool val)
    {
        onLedder = val;
    }

    public bool GetOnLedder()
    {
        return onLedder;
    }

    bool CanJumpAndMove()
    {
        var collider = collider2D;
        //две точки для определения хита внизу, чтобы если встал на краю, можно было двигаться        
        var offset = new Vector3(collider.bounds.extents.x - 0.08f, 0);
        var point1 = collider.bounds.center + offset;
        var point2 = collider.bounds.center - offset;
        var yOffset = collider.bounds.extents.y - 0.08f;
        point1.y -= yOffset;
        point2.y -= yOffset;

        var pointCenter = collider.bounds.center;
        pointCenter.y -= collider.bounds.extents.y;

        var filter = new ContactFilter2D() { useTriggers = false, layerMask = LayerMaskRaycastGround, useLayerMask = true };
        var hit1 = new RaycastHit2D[1];
        var hit2 = new RaycastHit2D[1];
        var hit3 = new RaycastHit2D[1];

        Physics2D.Raycast(point1, -transform.up, filter, hit1, 0.2f);
        Physics2D.Raycast(point2, -transform.up, filter, hit2, 0.2f);
        Physics2D.Raycast(pointCenter, -transform.up, filter, hit3, 0.2f);

        return hit1[0].transform || hit2[0].transform || hit3[0].transform;
    }

    private void PlayFootStep()
    {
        if (Time.time - footStepTime >= (!isRun ? FootStepInterval : footStepIntervalRun))
        {
            soundEffect.PlaySound("FootStep");
            footStepTime = Time.time;
        }
    }

    void Update()
    {
        if (!unit.IsDeath && !GameManager.Instance.IsPaused)
        {
            var horizontal = InputManager.Instance.GetActionAxis("walk", true);
            float jump = 0;
            var canJumpAndMove = CanJumpAndMove();
            if (onLedder)
            {
                var vert = InputManager.Instance.GetActionAxis("WalkUpDown");
                if (vert != 0)
                {
                    gameObject.layer = 12;
                    Vector3 vec = rigid.velocity;
                    vec = transform.up * vert * Time.deltaTime * SpeedWalk * 80;
                    rigid.velocity = vec;
                    // transform.position += transform.up * vert * Time.deltaTime * SpeedWalk;
                }
                else
                {
                    rigid.velocity = new Vector2(0, 0);
                }
                rigid.gravityScale = 0;
            }
            else
            {
                gameObject.layer = 8;
                rigid.gravityScale = 1.5f;

            }
            if (canJumpAndMove)
            {
                var isJump = InputManager.Instance.GetActionButton("jump", true);
                jump = isJump ? JumpForce : 0;
                if (jump != 0)
                {
                    anim.SetTrigger("Jump");
                    soundEffect.PlaySound("Jump");
                }
                var speed = InputManager.Instance.GetActionButton("run") ? SpeedRun : SpeedWalk;


                if (Mathf.Abs(horizontal) > 0.1)
                {
                    if (speed == SpeedRun)
                    {
                        anim.SetBool("Run", true);
                        isRun = true;
                    }
                    else
                    {
                        anim.SetBool("Run", false);
                        isRun = false;
                    }
                    anim.SetBool("Walk", true);
                    unit.lookDir = horizontal > 0 ? PlayerUnit.LookDir.Right : PlayerUnit.LookDir.Left;
                    transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, horizontal > 0 ? 0 : 180, transform.rotation.eulerAngles.z);
                    PlayFootStep();
                }
                else
                {
                    anim.SetBool("Walk", false);
                    anim.SetBool("Run", false);
                    speed = 0;
                }
                if (isJump)
                    rigid.AddRelativeForce(transform.up * jump * 80);

                var moveDir = isJump ? transform.right.x * speed * 1.35f : transform.right.x * speed;

                rigid.velocity = new Vector2(moveDir, rigid.velocity.y);
            }
            else
            {
                anim.SetBool("Walk", false);
                anim.SetBool("Run", false);
            }
        }
    }
}
