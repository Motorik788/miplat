﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcherEnemy : EnemyUnit
{
    public Arrow Arrow;
    public Transform ArrowSpawn;

    private Arrow arrow;

    protected override void EnemyBehavior()
    {
        base.EnemyBehavior();
        if (!isCalm)
        {
            var targetDistance = (Target.transform.position - transform.position).magnitude;
            var canMove = CanMove();
            if (targetDistance < WeaponDistance && !canMove)
            {
                anim.SetBool("Attak", true);
            }
            else if (!canMove)
            {
                anim.SetBool("Attak", false);
            }
        }
    }

    public override void ApplyDamage()
    {
        Shoot();
    }

    public void InstanceArrow()
    {
        if (ArrowSpawn)
        {
            arrow = Instantiate(Arrow, ArrowSpawn.position, Quaternion.FromToRotation(transform.right, Vector3.right));
            arrow.transform.SetParent(transform);
        }

    }

    void Shoot()
    {
        if (arrow != null)
        {
            arrow.IsShooted = true;
            arrow.transform.SetParent(null);
            arrow.info = new HitInfo() { From = this, Value = WeaponDamage,TypeDamage = HitInfo.DamageType.Projectile };
        }
    }
}
