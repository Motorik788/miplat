﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EnemyUnit : Unit, IApplyDamageble
{
    public Unit Target;
    public float SpeedMove;
    public float MinDistance;
    public LayerMask GroundCheckMask;
    public int XPForKill;

    protected bool isCalm = true;
    private PointToPointMove move;

    #region UIEnemyFields
    public Image ArmorIcon;
    public Image HealthImage;
    private CanvasGroup HealthAlpha;

    #endregion

    new void Start()
    {
        base.Start();
        if (ArmorIcon)
            ArmorIcon.enabled = false;
        if (HealthImage)
        {
            HealthAlpha = HealthImage.GetComponent<CanvasGroup>();
            HealthImage.enabled = false;
        }
        Target = GameObject.FindObjectOfType<PlayerUnit>();
        move = GetComponent<PointToPointMove>();
        if (IsDeath)
            Death();
    }


    bool flickBack = true;
    void HealthFlicker()
    {

        if (HealthAlpha.alpha > 0.2f && flickBack)
            HealthAlpha.alpha = Mathf.MoveTowards(HealthAlpha.alpha, 0, 0.03f);
        else if (flickBack)
            flickBack = false;
        else if (!flickBack && HealthAlpha.alpha < 1f)
            HealthAlpha.alpha = Mathf.MoveTowards(HealthAlpha.alpha, 1, 0.03f);
        else
            flickBack = true;
    }

    void Update()
    {
        if (!IsDeath)
        {
            HealthImage.fillAmount = Health / MaxHealth;
            if (HealthImage.enabled)
                HealthFlicker();
            EnemyBehavior();
        }
        else
            Death();
    }

    protected void Agry()
    {
        isCalm = false;
        AgryTimer = 7;
        if (move && move.IsMoving)
            move.StopMove();
    }

    float AgryTimer;
    protected virtual void EnemyBehavior()
    {
        if (isCalm)
        {
            if (VisibleTarget())
            {
                Agry();
                if (move && move.enabled)
                    move.StopMove();
            }
            else
            {
                if (move && move.enabled && !move.IsMoving)
                {
                    move.StartMove();
                }
            }
        }
        else
        {
            RotateEnemyToTarget(delay: 1f);
            var canMove = CanMove();
            var visible = VisibleTarget();
            var targetDistance = (Target.transform.position - transform.position).magnitude;

            if (targetDistance > WeaponDistance && canMove)
            {
                rigid.velocity = transform.right * SpeedMove * Time.deltaTime;
                anim.SetBool("Attak", false);
                anim.SetBool("Walk", true);
            }
            else if (targetDistance < MinDistance && CanMoveBack())
            {
                anim.SetBool("Attak", false);
                anim.SetBool("Walk", true);
                rigid.velocity = -transform.right * SpeedMove * Time.deltaTime;
            }
            else if (canMove && visible)
            {

                anim.SetBool("Attak", true);
                anim.SetBool("Walk", false);
            }
            else
            {
                anim.SetBool("Attak", false);
                anim.SetBool("Walk", false);
            }
            if (!visible)
            {
                AgryTimer -= Time.deltaTime;
                if (AgryTimer <= 0)
                    isCalm = true;
            }
            else
                AgryTimer = 7;
        }
    }

    public bool CanMove()
    {
        var rayHit = Physics2D.Raycast(transform.position + transform.right / 2.2f, -transform.up, 1f, GroundCheckMask);
        return rayHit.transform ? true : false;
    }

    public bool CanMoveBack()
    {
        var rayHit = Physics2D.Raycast(transform.position - transform.right / 2.2f, -transform.up, 1f, GroundCheckMask);
        var rayHit2 = Physics2D.Raycast(transform.position - transform.up / 2, -transform.right, 0.5f, GroundCheckMask);
        return rayHit.transform && !rayHit2.transform ? true : false;
    }

    bool rotateCoroutine = false;
    Coroutine coroutineRotate;
    /// <summary>
    /// Поворот в сторону цели в с задержкой и без, если есть задержка то запускается корутина (защита от множественной корутины присутствует).
    /// При пофроте без задержки, во избежание конфликата корутины и обычного поворота, предваритеьно останавливается корутина.
    /// </summary>
    /// <param name="target"></param>
    /// <param name="delay"></param>
    public void RotateEnemyToTarget(GameObject target = null, float delay = 0)
    {
        if (delay > 0)
        {
            if (!rotateCoroutine)
                coroutineRotate = StartCoroutine(RotateToCoroutine(target, delay));
        }
        else
        {
            if (rotateCoroutine)
            {
                StopCoroutine(coroutineRotate);
                rotateCoroutine = false;
            }
            RotateTo(target);
        }
    }

    protected bool VisibleTarget()
    {
        var rayHit = Physics2D.Raycast(transform.position, transform.right, WeaponDistance * 1.5f, LayerMaskAttak);
        if (rayHit.transform && rayHit.transform == Target.transform)
            return true;
        else return false;
    }

    protected void RotateTo(GameObject target = null)
    {
        var trgt = target ? target : Target.gameObject;
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.position.x < trgt.transform.position.x ? 0 : 180, transform.rotation.eulerAngles.z);
    }

    protected IEnumerator RotateToCoroutine(GameObject target = null, float delay = 0)
    {
        rotateCoroutine = true;
        yield return new WaitForSeconds(delay);
        RotateTo(target);
        rotateCoroutine = false;
    }

    protected IEnumerator HealthShow()
    {
        HealthImage.enabled = true;
        HealthAlpha.alpha = 1;
        if (ArmorIcon && Armor > 0)
            ArmorIcon.enabled = true;
        else
            ArmorIcon.enabled = false;
        yield return new WaitForSeconds(5f);
        HealthImage.enabled = false;
        if (ArmorIcon)
            ArmorIcon.enabled = false;
    }

    Coroutine healthFlick;

    public override bool Damage(HitInfo info)
    {
        if (base.Damage(info))
        {
            if (info.From != null && info.From == GameManager.Instance.Player)
            {
                if (Health <= 0)
                {
                    GameManager.Instance.AddXp(XPForKill);
                    var te = ObjectPool.Instance.Create(GameManager.Instance.TextXPKillPrefab);
                    te.text = string.Format("+{0}xp", XPForKill);
                    te.transform.position = transform.position;
                    te.GetComponent<ObjectMoveTo>().Move();
                }

                Agry();
            }

            if (!IsDeath)
            {
                if (healthFlick != null)
                    StopCoroutine(healthFlick);
                if (coroutineRotate != null && (info.From == null || (info.From != null && info.From != Target)))
                {
                    StopCoroutine(coroutineRotate);
                    rotateCoroutine = false;
                }
                healthFlick = StartCoroutine(HealthShow());
            }
            return true;
        }
        return false;
    }

    public override void Death()
    {
        base.Death();
        anim.SetBool("Walk", false);
        anim.SetTrigger("Dead");
        GetComponent<Collider2D>().enabled = false;
        rigid.simulated = false;
        transform.position -= transform.up * 0.15f;
        enabled = false;
        var spriteRend = GetComponent<SpriteRenderer>();
        spriteRend.sortingLayerName = "Player";
        spriteRend.sortingOrder = 3;
        if (ArmorIcon)
            ArmorIcon.enabled = false;
        if (healthFlick != null)
            StopCoroutine(healthFlick);
        if (HealthImage)
            HealthImage.enabled = false;
    }

    public virtual void ApplyDamage()
    {
        ApplyDamageHelper.ApplyDamageClose(this);
    }
}
