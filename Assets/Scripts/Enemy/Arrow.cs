﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    public bool IsShooted;
    public float Speed;
    public float LifeTime;
    public HitInfo info;
    public LayerMask HitMask;

    void Update()
    {
        if (LifeTime > 0)
        {
            LifeTime -= Time.deltaTime;
            if (IsShooted)
            {
                transform.position += transform.right * Speed * Time.deltaTime;
                Hit();
            }
        }
        else
            DestroyImmediate(gameObject);
    }


    void Hit()
    {
        var hit = Physics2D.Raycast(transform.position, transform.forward, 1, HitMask);
        if (hit.transform)
        {
            var unit = hit.transform.GetComponent<Unit>();
            if (unit)
            {
                unit.Damage(info);
                IsShooted = false;
                transform.SetParent(unit.transform);
                Destroy(gameObject,2f);
            }
        }
    }
}
