﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyUnit))]
public class PointToPointMove : MonoBehaviour
{
    public PointToMove[] Path;

    public bool IsMoving
    {
        get
        {
            return isMoving;
        }
    }

    private bool isMoving;
    private int currentPoint = -1;
    private EnemyUnit unit;
    private float speed;

    void Start()
    {
        unit = GetComponent<EnemyUnit>();
        speed = unit.SpeedMove / 2;
        if (SaveManager.Instance)
            SaveManager.Instance.Loaded += Instance_Loaded;
    }

    private void Instance_Loaded()
    {
        GetNextPoint();
        unit.RotateEnemyToTarget(Path[currentPoint].gameObject);
        closePoint = false;
    }

    private void OnDestroy()
    {
        if (SaveManager.Instance != null)
            SaveManager.Instance.Loaded -= Instance_Loaded;
    }

    public void StartMove()
    {
        isMoving = true;
        unit.anim.SetBool("Walk", true);
    }

    public void StopMove()
    {
        isMoving = false;
        unit.anim.SetBool("Walk", false);
        closePoint = true;
    }


    void Update()
    {
        if (isMoving && Path != null && Path.Length > 1)
            Move();
    }


    float waitTimer;
    bool closePoint = true;
    bool prepareTojump;

    void Move()
    {
        if (closePoint && waitTimer > 0)
            waitTimer -= Time.deltaTime;

        if (closePoint && waitTimer <= 0)
        {
            GetNextPoint();
            unit.RotateEnemyToTarget(Path[currentPoint].gameObject);
            closePoint = false;
            unit.anim.SetBool("Walk", true);
        }
        if (!closePoint)
        {
            if ((Path[currentPoint].transform.position - transform.position).magnitude > 0.5f)
                unit.rigid.velocity = transform.right * speed * Path[currentPoint].Speed * Time.deltaTime;
            else
            {
                closePoint = true;
                waitTimer = Path[currentPoint].Wait;
                unit.anim.SetBool("Walk", false);
            }
        }
    }

    bool back = false;

    void GetNextPoint()
    {
        if (!back)
        {
            currentPoint++;
            if (currentPoint >= Path.Length)
                back = true;
        }
        if (back)
        {
            currentPoint--;
            if (currentPoint < 0)
            {
                back = false;
                currentPoint++;
            }
        }
    }
}
