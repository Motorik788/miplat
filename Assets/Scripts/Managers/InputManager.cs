﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class ActionInputWindows
{
    public string Name;
    public KeyCode Key;
    public string AxisName;
    //public bool IsButton;
    public bool IsAxis;
    public bool IsRawAxis;
}


public class InputManager : MonoBehaviour
{
    public bool IsWindows = true;

    public ActionInputWindows[] ActionsWindows;


    public static InputManager Instance
    {
        get
        {
            return instance;
        }
    }

    private static InputManager instance;

    private UnityEngine.EventSystems.EventSystem eventSystem;

    //public InputManager()
    //{
    //    if (instance == null)
    //        instance = this;
    //}

    private void Awake()
    {
        if (instance == null)
            instance = this;

        if (instance != this)
            DestroyImmediate(this);
    }

    public bool GetActionButton(string name, bool isButtonDown = false)
    {
        name = name.ToLower();
        var act = ActionsWindows.FirstOrDefault((x) => x.Name.ToLower() == name);
        if (act != null)
        {
            if (IsWindows)
            {
                //if (eventSystem.IsPointerOverGameObject() && IsWindowsMouse(act.Key))
                    //return false;
                return isButtonDown ? Input.GetKeyDown(act.Key) : Input.GetKey(act.Key);
            }
            else
            {
                return false;
            }
        }
        else
            return false;
    }

    private bool IsWindowsMouse(KeyCode key)
    {
        return key == KeyCode.Mouse0 ||
            key == KeyCode.Mouse1 ||
            key == KeyCode.Mouse2 ||
            key == KeyCode.Mouse3 ||
            key == KeyCode.Mouse4 ||
            key == KeyCode.Mouse5 ||
            key == KeyCode.Mouse6;
    }

    public float GetActionAxis(string name, bool IsRaw = false)
    {
        name = name.ToLower();
        var act = ActionsWindows.FirstOrDefault((x) => x.Name.ToLower() == name && x.IsAxis && x.IsRawAxis == IsRaw);
        if (act != null)
        {
            if (IsWindows)
                return IsRaw ? Input.GetAxisRaw(act.AxisName) : Input.GetAxis(act.AxisName);
            else
            {
                return 0;
            }
        }
        else
            return 0;
    }


    void Start()
    {
        eventSystem = UnityEngine.EventSystems.EventSystem.current;
    }

    //void Update()
    //{

    //}
}
