﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class SoundSettings
{
    public float MusicVolume;
    public float EffectVolume;
    public UnityEventParam MusicVolumeChanged;
    public UnityEventParam EffectVolumeChanged;
    public AudioSource SourceMusic;

    public void PlayMusic(AudioClip clip)
    {
        if (SourceMusic)
        {
            SourceMusic.clip = clip;
            SourceMusic.volume = MusicVolume;
            SourceMusic.Play();
        }
    }


    [System.Serializable]
    public class UnityEventParam : UnityEvent<float> { }

}




public class GameSettingsSingleton : MonoBehaviour
{
    public SoundSettings SoundSettings;


    private static GameSettingsSingleton instance;

    public static GameSettingsSingleton Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {

        if (instance == null)
            instance = this;
        else
            Destroy(this);
        DontDestroyOnLoad(this);
    }



    void Start()
    {
        SaveManager.Instance.Loaded += Instance_Loaded;
    }

    private void Instance_Loaded()
    {

        SoundSettings.MusicVolumeChanged.Invoke(SoundSettings.MusicVolume);
        SoundSettings.EffectVolumeChanged.Invoke(SoundSettings.EffectVolume);
    }


    //void Update()
    //{

    //}
}
