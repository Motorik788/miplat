﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    public PlayerUnit Player;
    public int XP;
    public int DeathOnLevel;
    public int XpOnLevel;
    public GameMusicPlayer GameMusicPlayer;
    public LevelsManager LevelsManager;
    public Texture2D cursor;
    public TextMeshPro TextXPKillPrefab;
    public bool IsPaused;

    [SceneDrawer]
    public int CurrentLevel;

    private int deathOnLevel;


    public static GameManager Instance
    {
        get
        {
            return instance;
        }
    }

    private static GameManager instance;

    public void Awake()
    {
        if (instance != null && instance != this)
            Destroy(gameObject);
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    void Start()
    {
        if (!Player)
            Player = GameObject.FindObjectOfType<PlayerUnit>();
        UnityEngine.SceneManagement.SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        if (!LevelsManager)
            LevelsManager = GetComponent<LevelsManager>();
        if (cursor)
            Cursor.SetCursor(cursor, Vector2.zero, CursorMode.ForceSoftware);
        SaveManager.Instance.Loaded += Instance_Loaded;
    }

    private void Instance_Loaded()
    {
        var lvlM = LevelsManager.Instance;
        //костыль, чтобы правильно считать количество смертей на уровне, количесво смертей должны сохраняться как global
        if (lvlM.CurrentLevel == lvlM.MenuLevel)
            deathOnLevel = DeathOnLevel;

        if (lvlM.CurrentLevel != lvlM.MenuLevel && lvlM.CurrentLevel != lvlM.SceneLoader)
        {
            CurrentLevel = lvlM.CurrentLevel;
            DeathOnLevel = deathOnLevel;
        }
        SaveManager.Instance.Save();
    }

    public void AddXp(int xp)
    {
        XP += xp;
        XpOnLevel += xp;
    }

    public void PlayerDeathInc()
    {
        deathOnLevel++;
        DeathOnLevel++;
    }

    private void SceneManager_sceneLoaded(UnityEngine.SceneManagement.Scene arg0, UnityEngine.SceneManagement.LoadSceneMode arg1)
    {
        if (!Player)
            Player = GameObject.FindObjectOfType<PlayerUnit>();
        var lvlM = LevelsManager.Instance;
        if (arg0.buildIndex != lvlM.SceneLoader)
            SaveManager.Instance.Load();
    }

    public void GamePause()
    {
        Time.timeScale = 0;
        IsPaused = true;
    }

    public void GameContinue()
    {
        Time.timeScale = 1;
        IsPaused = false;
    }

    public void ClearLevelStats()
    {
        DeathOnLevel = 0;
        XpOnLevel = 0;
        deathOnLevel = 0;
    }

    public void LoadLastSave()
    {
        if (CurrentLevel != 0)
        {
            LevelsManager.Instance.LoadLevel(CurrentLevel);
        }
        else
        {
            LevelsManager.Instance.LoadFirstLevel();
        }
    }
}
