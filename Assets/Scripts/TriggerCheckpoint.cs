﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ComponScan))]
public class TriggerCheckpoint : TriggerAction2D
{

    public void Save()
    {
        IsActive = false;
        SaveManager.Instance.Save();
    }

    new void Update()
    {
        if (IsActive)
        {
            base.Update();
        }
        else
        {
            if ((GameManager.Instance.Player.transform.position - transform.position).magnitude > 6)
                IsActive = true;
        }
    }
}
