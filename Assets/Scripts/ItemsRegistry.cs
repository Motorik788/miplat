﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsRegistry : MonoBehaviour
{
    [SerializeField]
    private List<ObjectInfo> Objects = new List<ObjectInfo>();

    public static ItemsRegistry Instance
    {
        get
        {
            return instance;
        }
    }

    private static ItemsRegistry instance;

    public void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }


    public bool Contains(ObjectInfo objectInfo)
    {
        return Objects.Contains(objectInfo);
    }

    public ObjectInfo GetObjectByIndex(int index)
    {
        return Objects[index];
    }

    public int GetIndexByObject(ObjectInfo info)
    {
        return Objects.IndexOf(info);
    }

    //void Start()
    //{

    //}


    //void Update()
    //{

    //}
}
