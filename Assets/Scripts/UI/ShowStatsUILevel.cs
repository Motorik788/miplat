﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowStatsUILevel : MonoBehaviour
{
    public Text DeathText;
    public Text Xp;
    public Text LevelCompliteXp;
    // public Text AllXp;
    public TextIntLerp AllXp;
    public UnityEngine.PostProcessing.PostProcessingProfile Effectprofile;


    public void Show(bool endLevel = true)
    {
        gameObject.SetActive(true);
        Effectprofile.depthOfField.enabled = true;
        GameManager.Instance.GamePause();
        if (endLevel)
            GameManager.Instance.AddXp(200);
        DeathText.text = "Смертей: " + GameManager.Instance.DeathOnLevel.ToString();
        Xp.text = "XP: +" + GameManager.Instance.XpOnLevel.ToString();
        if (LevelCompliteXp && endLevel)
            LevelCompliteXp.text = "За прохождение уровня +200XP";
        UpdateTextAllXp();
    }

    public void UpdateTextAllXp()
    {
       
        //AllXp.text = "XP: " + GameManager.Instance.XP.ToString();
        if(!AllXp.Animate("XP: {0}",-1, GameManager.Instance.XP,2f))
            AllXp.Text.text = "XP: " + GameManager.Instance.XP.ToString();
    }

    public void Close()
    {
        GameManager.Instance.GameContinue();
        Effectprofile.depthOfField.enabled = false;
        gameObject.SetActive(false);
    }

    public void OnDestroy()
    {
        Effectprofile.depthOfField.enabled = false;
    }
}
