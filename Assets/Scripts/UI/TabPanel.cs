﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;


#if UNITY_EDITOR

[CustomEditor(typeof(TabPanel))]
public class TabPanelEditor : Editor
{

    TabPanel tabPanel;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (!tabPanel)
            tabPanel = target as TabPanel;
        if (GUILayout.Button("Create Tabs"))
        {
            ClearPanels();
            Generate();
        }
    }


    public void Generate()
    {
        var tabOffset = tabPanel.TabPrefab.targetGraphic.rectTransform.rect.width - tabPanel.TabOffsetX;
        var pos = Vector3.zero;
        for (int i = 0; i < tabPanel.TabsCount; i++)
        {
            var but = Instantiate(tabPanel.TabPrefab, tabPanel.TabsContainer.transform);
            but.gameObject.SetActive(true);
            if (i == 0)
            {
                pos.x = but.targetGraphic.rectTransform.rect.width / 2;
                pos.y = -but.targetGraphic.rectTransform.rect.height / 2;
            }
            else
                pos.x += tabOffset;
            but.targetGraphic.rectTransform.anchoredPosition3D = pos;
            but.transform.SetAsFirstSibling();
            but.name = i.ToString();
            UnityEditor.Events.UnityEventTools.AddObjectPersistentListener(but.onClick, tabPanel.SetCurrentTab, but);

            var scrolView = Instantiate(tabPanel.ScrollViewPrefab, tabPanel.PanelsContainer.transform);
            scrolView.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
            tabPanel.ScrollPanelsContent.Add(scrolView.transform.GetChild(0).GetChild(0).gameObject);
            tabPanel.ScrollViews.Add(scrolView);
        }
    }



    public void ClearPanels()
    {
        tabPanel.ScrollPanelsContent.Clear();
        for (int i = 0; i < tabPanel.ScrollViews.Count; i++)
        {
            DestroyImmediate(tabPanel.ScrollViews[i]);
        }
        tabPanel.ScrollViews.Clear();

        while (tabPanel.TabsContainer.transform.childCount > 0)
        {
            DestroyImmediate(tabPanel.TabsContainer.transform.GetChild(0).gameObject);
        }
    }
}

#endif


public class TabPanel : MonoBehaviour
{
    public int TabsCount;
    public int TabOffsetX = 20;
    public int TabSelectSizeYOffset = 20;
    public GameObject TabsContainer;
    public GameObject PanelsContainer;
    public GameObject ScrollViewPrefab;
    public Button TabPrefab;


    [HideInInspector]
    public Button currentTab;
    [HideInInspector]
    public List<GameObject> ScrollPanelsContent = new List<GameObject>();
    [HideInInspector]
    public List<GameObject> ScrollViews = new List<GameObject>();

    private Button prevTab = null;

    void Start()
    {
        if (!TabsContainer)
            Debug.LogError("TabsContainer is null!");
        if (!PanelsContainer)
            Debug.LogError("PanelsContainer is null!");
        if (!ScrollViewPrefab)
            Debug.LogError("ScrollViewPrefab is null!");
        if (!TabPrefab)
            Debug.LogError("TabPrefab is null!");
        if (ScrollPanelsContent.Count == 0)
            currentTab = null;

        currentTab = TabsContainer.transform.GetChild(TabsContainer.transform.childCount - 1).GetComponent<Button>();

        OpenCurrentTab();
    }

    public void SetCurrentTab(Button nCur)
    {
        if (nCur != currentTab)
        {
            currentTab = nCur;
            OpenCurrentTab();
        }
    }

    public void OpenCurrentTab()
    {
        if (currentTab != null)
        {
            ClosePrevTab();
            prevTab = currentTab;
            var rect = prevTab.targetGraphic.rectTransform;
            rect.sizeDelta = new Vector2(rect.sizeDelta.x, rect.sizeDelta.y + TabSelectSizeYOffset);
            var pos = prevTab.targetGraphic.rectTransform.anchoredPosition;
            pos = new Vector2(pos.x, pos.y - TabSelectSizeYOffset / 2);//делить на 2 не знаю почему именно, но так надо
            prevTab.targetGraphic.rectTransform.anchoredPosition = pos;
            prevTab.transform.SetAsLastSibling();
            int indexPanel = int.Parse(prevTab.name);
            ScrollViews[indexPanel].SetActive(true);
        }
    }

    public void ClosePrevTab()
    {
        if (prevTab)
        {
            var rect = prevTab.targetGraphic.rectTransform;
            rect.sizeDelta = new Vector2(rect.sizeDelta.x, rect.sizeDelta.y - TabSelectSizeYOffset);
            var pos = prevTab.targetGraphic.rectTransform.anchoredPosition;
            pos = new Vector2(pos.x, pos.y + TabSelectSizeYOffset / 2);//делить на 2 не знаю почему именно, но так надо
            prevTab.targetGraphic.rectTransform.anchoredPosition = pos;
            int indexPanel = int.Parse(prevTab.name);
            prevTab.transform.SetSiblingIndex(ScrollViews.Count - 1 - indexPanel);
            ScrollViews[indexPanel].SetActive(false);
        }
    }

    public void AddItem(int panelIndex, GameObject item)
    {      
        item.transform.SetParent(ScrollPanelsContent[panelIndex].transform);
        item.transform.localScale = Vector3.one;
    }
}
