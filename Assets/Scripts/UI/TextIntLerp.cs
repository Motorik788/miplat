﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextIntLerp : MonoBehaviour
{
    public Text Text;


    private int val;
    private string formatString;
    private int targetVal;

    private bool active;
    private float delta;

    public bool Animate(string formatString, int from, int to, float time)
    {
        if (from > 0)
            val = from;
        targetVal = to;
        this.formatString = formatString;
        if (gameObject.activeInHierarchy)
        {           
            active = true;
            delta = Mathf.Abs(targetVal - val) / time;
        }
        else
            val = targetVal;
        return gameObject.activeInHierarchy;
    }

    
    void Start()
    {
        if (!Text)
            Text = GetComponent<Text>();
    }

    void Update()
    {
        if (active)
        {
            val = (int)Mathf.MoveTowards(val, targetVal, Time.unscaledDeltaTime * delta);
            if (val == targetVal)
                active = false;
            Text.text = string.Format(formatString, val);
        }
    }
}
