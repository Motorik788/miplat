﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowChestItems : MonoBehaviour
{
    public GameObject PanelContent;
    public InventarItemUI itemUIPrefab;


    public void Show(IEnumerable<InventarCell> inventarCells)
    {
        gameObject.SetActive(true);
        for (int i = 0; i < PanelContent.transform.childCount; i++)
        {
            Destroy(PanelContent.transform.GetChild(i).gameObject);
        }

        foreach (var item in inventarCells)
        {
            var UiItem = Instantiate(itemUIPrefab);
            UiItem.Info = item.Object;
            UiItem.transform.SetParent(PanelContent.transform);
            UiItem.transform.localScale = Vector3.one;
            UiItem.Count.text = item.Count.ToString();
            UiItem.CanUse = false;
            var rectTr = UiItem.GetComponent<RectTransform>();
            rectTr.sizeDelta = new Vector2(rectTr.sizeDelta.x / 2, rectTr.sizeDelta.y - 20);
            UiItem.Description.gameObject.SetActive(false);

        }
    }

}
