﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuUI : MonoBehaviour
{
    public Image SettingsPanel;
    public Text SettingMusicValueText;
    public Text SettingEffectValueText;


    private PointToPointAnimatePos SettingAnimator;

    void Start()
    {
        SaveManager.Instance.Load();
        SaveManager.Instance.Loaded += Instance_Loaded;
        if (SettingsPanel)
            SettingAnimator = SettingsPanel.GetComponent<PointToPointAnimatePos>();

    }

    public void LoadLastSave()
    {
        GameManager.Instance.LoadLastSave();
    }

    public void DeleteAllSaveData()
    {
        SaveManager.Instance.ClearSave();
        GameManager.Instance.XP = 0;
        GameManager.Instance.CurrentLevel = 0;
    }
    
    private void Instance_Loaded()
    {
        if (SettingMusicValueText)
        {
            var val = GameSettingsSingleton.Instance.SoundSettings.MusicVolume;
            SettingMusicValueText.text = ValToPercent(val);
            SettingMusicValueText.transform.parent.GetComponent<Slider>().value = val;
        }
        if (SettingEffectValueText)
        {
            var val = GameSettingsSingleton.Instance.SoundSettings.EffectVolume;
            SettingEffectValueText.text = ValToPercent(val);
            SettingEffectValueText.transform.parent.GetComponent<Slider>().value = val;
        }
    }

    public void Exit()
    {
        Application.Quit();
    }


    //void Update()
    //{

    //}

    string ValToPercent(float val)
    {
        return (val * 100).ToString("0") + "%";
    }

    public void MusicSlider(float val)
    {
        if (SettingMusicValueText)
            SettingMusicValueText.text = ValToPercent(val);
        GameSettingsSingleton.Instance.SoundSettings.MusicVolume = val;
        GameSettingsSingleton.Instance.SoundSettings.MusicVolumeChanged.Invoke(val);
    }

    public void EffectSlider(float val)
    {
        if (SettingEffectValueText)
            SettingEffectValueText.text = ValToPercent(val);
        GameSettingsSingleton.Instance.SoundSettings.EffectVolume = val;
        GameSettingsSingleton.Instance.SoundSettings.EffectVolumeChanged.Invoke(val);
    }


    public void OpenCloseSettingsHandler()
    {
        if (SettingAnimator)
            SettingAnimator.Play();
    }

    public void CloseSettingSave()
    {
        if (SettingAnimator && SettingAnimator.IsEnd)
        {
            SaveManager.Instance.Save();
        }
    }
}
