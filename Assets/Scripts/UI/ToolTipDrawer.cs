﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ToolTipDrawer : MonoBehaviour
{
    public bool IsActive;
    public Image Panel;
    public Text TextToolTip;

    private bool isShow;
    private RectTransform rectTransformCanvas;
    private float originalWidth;

    private void Start()
    {
        drawer = this;
        rectTransformCanvas = Panel.canvas.transform as RectTransform;
        originalWidth = Panel.rectTransform.rect.width;
    }

    public void Show(string text, float width = -1)
    {
        Panel.gameObject.SetActive(true);
        TextToolTip.text = text;
        isShow = true;
        Panel.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, TextToolTip.preferredHeight + 10);
        if (width > 0)
        {
            Panel.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
        }
    }

    public void Close()
    {
        if (Panel)
        {
            Panel.gameObject.SetActive(false);
            Panel.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, originalWidth);
        }
        isShow = false;
    }

    private static ToolTipDrawer drawer;

    public static ToolTipDrawer Instance
    {
        get
        {
            return drawer;
        }
    }

    void Update()
    {
        if (IsActive)
        {
            if (isShow)
            {
                Vector2 pos;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransformCanvas, Input.mousePosition, Panel.canvas.worldCamera, out pos);
                var p = Panel.canvas.transform.TransformPoint(pos);
                Panel.transform.position = p;
            }
        }
    }
}
