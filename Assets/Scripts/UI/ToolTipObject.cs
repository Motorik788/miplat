﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ToolTipObject : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public string Text;
    [Header("-1 значит не переопределять ширину")]
    public float OverrideWidth = -1;
    [Header("Минимальная дистанция учитывается если выбран таргет объект")]
    public Transform ObjectToDistance;
    public float MinDistance;

    private bool show;

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!ObjectToDistance || (ObjectToDistance && (ObjectToDistance.position - transform.position).magnitude <= MinDistance))
        {
            ToolTipDrawer.Instance.Show(Text, OverrideWidth);
            show = true;
        }

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ToolTipDrawer.Instance.Close();
        show = false;
    }

    private void OnDestroy()
    {
        Close();
    }

    public void Close()
    {
        ToolTipDrawer.Instance.Close();
        show = false;
    }

    private void OnDisable()
    {
        Close();
    }

    public void Update()
    {
        if (ObjectToDistance && (ObjectToDistance.position - transform.position).magnitude > MinDistance && show)
        {
            Close();
        }
    }
}
