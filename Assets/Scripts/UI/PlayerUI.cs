﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(PlayerUnit))]
public class PlayerUI : MonoBehaviour
{
    public Image HealthImage;
    public Image ArmorImage;
    public TabPanel InventarPanel;
    public InventarItemUI InventarItemPrefab;
    public Image CurrentThrowable;
    public ShowChestItems PanelChestItems;
    public Image SaveIcon;
    public ShowStatsUILevel ShowStats;

    private PlayerUnit unit;
    private Dictionary<int, InventarItemUI> inventarDict = new Dictionary<int, InventarItemUI>();

    private Color colorImageThrowableOff;
    private Color colorImageThrowableOn;

    void Start()
    {
        unit = GetComponent<PlayerUnit>();
        unit.InventarEvent += AddRemoveItemInPanel;
        unit.ChangeCurrentThrowable += Unit_ChangeCurrentThrowable;
        colorImageThrowableOff = CurrentThrowable.color;
        colorImageThrowableOn = new Color(colorImageThrowableOff.r, colorImageThrowableOff.g, colorImageThrowableOff.b, 1);
        CurrentThrowable.preserveAspect = true;
    }

    private void Unit_ChangeCurrentThrowable(TrowableObject obj)
    {
        if (obj != null)
        {
            CurrentThrowable.sprite = obj.SpriteIcon;
            CurrentThrowable.color = colorImageThrowableOn;
        }
        else
            CurrentThrowable.color = colorImageThrowableOff;
    }

    public void OpenCloseInventar()
    {
        if (InventarPanel)
        {
            var UiEnable = InventarPanel.gameObject.activeInHierarchy;
            if (UiEnable)
                GameManager.Instance.GameContinue();
            else
                GameManager.Instance.GamePause();
            InventarPanel.gameObject.SetActive(!InventarPanel.gameObject.activeInHierarchy);
        }
    }

    public void SaveIconFlick()
    {
        if (SaveIcon)
            StartCoroutine(FlickSaveIcon());
    }

    IEnumerator FlickSaveIcon()
    {
        Color color = SaveIcon.color;
        for (int j = 0; j < 3; j++)
        {
            for (float i = 0.1f; i < 1; i += 0.1f)
            {
                color.a = i;
                SaveIcon.color = color;
                yield return new WaitForSecondsRealtime(0.05f);
            }
            for (float i = 1; i > 0; i -= 0.1f)
            {
                color.a = i;
                SaveIcon.color = color;
                yield return new WaitForSecondsRealtime(0.05f);
            }
        }
        color.a = 0;
        SaveIcon.color = color;
    }

    public void ClickPause()
    {
        GameManager.Instance.GamePause();
    }

    public void ClickContinue()
    {
        GameManager.Instance.GameContinue();
    }

    public void ShowChestItems(IEnumerable<InventarCell> inventarCells)
    {
        PanelChestItems.Show(inventarCells);
        GameManager.Instance.GamePause();
    }

    public void ShowStatsLevel(bool endLevel = true)
    {
        ShowStats.Show(endLevel);

    }

    int preparedLevel = -1;
    public void PrepareLoadLevel(int level)
    {
        preparedLevel = level;
    }

    public void LoadPreparedLevel()
    {
        if (preparedLevel != -1)
        {
            GameManager.Instance.ClearLevelStats();
            SaveManager.Instance.Save();
            LevelsManager.Instance.LoadLevel(preparedLevel);
        }
    }

    public void ExitToMenu()
    {
        GameManager.Instance.GameContinue();
        LevelsManager.Instance.LoadMenuLevel();
    }

    public void AddRemoveItemInPanel(InventarCell item, PlayerUnit.InventarItemAction itemAction)
    {
        if (InventarItemPrefab && InventarPanel)
        {
            var id = item.Object.GetHashCode();
            if (itemAction == PlayerUnit.InventarItemAction.Add)
            {

                if (!inventarDict.ContainsKey(id))
                {
                    var UiItem = Instantiate(InventarItemPrefab);
                    inventarDict[id] = UiItem;
                    UiItem.Info = item.Object;
                    UiItem.CanUse = true;

                    int idPanel = 0;
                    switch (item.Object.Type)
                    {
                        case ObjectInfo.ObjectType.Eat:
                            idPanel = 1;
                            break;
                        case ObjectInfo.ObjectType.Potion:
                            idPanel = 0;
                            break;
                        case ObjectInfo.ObjectType.Trowable:
                            idPanel = 2;
                            break;
                        default:
                            idPanel = 3;
                            break;
                    }

                    InventarPanel.AddItem(idPanel, UiItem.gameObject);
                }
                inventarDict[id].Count.text = item.Count.ToString();
            }
            else
            {
                if (item.Count <= 0)
                {
                    var go = inventarDict[id];
                    inventarDict.Remove(id);
                    Destroy(go.gameObject);
                }
                else
                {
                    inventarDict[id].Count.text = item.Count.ToString();
                }
            }
        }
    }

    void Update()
    {
        if (HealthImage)
            HealthImage.fillAmount = unit.Health / unit.MaxHealth;
        if (ArmorImage)
            ArmorImage.fillAmount = unit.Armor / unit.MaxArmor;
        if (InputManager.Instance.GetActionButton("OpenInventar", true))
        {
            OpenCloseInventar();
        }
        if (InputManager.Instance.GetActionButton("OpenSkill", true) && !ShowStats.gameObject.activeSelf)
        {
            ShowStatsLevel(false);
        }
    }
}
