﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointToPointAnimatePos : MonoBehaviour
{
    public Vector3 StartPos;
    public Vector3 EndPos;
    public float Speed;

    public bool IsEnd
    {
        get
        {
            return isStarting;
        }
    }

    private bool isStarting;
    private bool isPlay;
    private Vector3 velocity;

    void Start()
    {
        transform.position = StartPos;
    }


    void Update()
    {
        if (isPlay)
        {
            if (isStarting)
            {
                transform.position = Vector3.MoveTowards(transform.position, StartPos, Speed);
                if ((StartPos - transform.position).magnitude <= 0.1)
                {
                    isStarting = false;
                    isPlay = false;
                }
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, EndPos, Speed);
                if ((EndPos - transform.position).magnitude <= 0.1)
                {
                    isStarting = true;
                    isPlay = false;
                }
            }
        }
    }

    public void Play()
    {
        isPlay = true;
    }
}
