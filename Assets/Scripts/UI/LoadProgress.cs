﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LoadProgress : MonoBehaviour
{
    public Image ProgressBar;

    LevelsManager LevelsManager;

    void Start()
    {
        LevelsManager = GameManager.Instance.LevelsManager;
    }

    // Update is called once per frame
    void Update()
    {
        if (ProgressBar)
        {
            if (LevelsManager.loadOperation != null)
                ProgressBar.fillAmount = LevelsManager.loadOperation.progress;
        }
    }
}
