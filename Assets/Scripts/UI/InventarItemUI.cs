﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InventarItemUI : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
    public ObjectInfo Info;
    public Image Icon;
    public Text Name;
    public Text Description;
    public Text Count;
    public Image UsingProgress;
    public bool CanUse = true;
    public float SpeedUse = 0.5f;

    [HideInInspector]
    public PlayerUI PlayerUI;

    private bool startedUse;




    public void OnPointerDown(PointerEventData eventData)
    {
        startedUse = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        startedUse = false;
        UsingProgress.fillAmount = 0;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        startedUse = false;
        UsingProgress.fillAmount = 0;
    }

    void Start()
    {
        if (Info)
        {
            Description.text = Info.ToString();
            Name.text = Info.Name;
            Icon.sprite = Info.SpriteIcon;
            Icon.preserveAspect = true;
            if (!string.IsNullOrEmpty(Description.text))
            {
                (transform as RectTransform).SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Description.preferredHeight + Name.preferredHeight + 40);
                Description.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Description.preferredHeight);
            }
        }
    }


    void Update()
    {
        if (startedUse && UsingProgress && CanUse)
        {
            if (UsingProgress.fillAmount < 1)
                UsingProgress.fillAmount += Time.unscaledDeltaTime * SpeedUse;
            else
            {
                startedUse = false;
                UsingProgress.fillAmount = 0;
                if (Info.Type != ObjectInfo.ObjectType.Trowable)
                {
                    Info.Use();
                    GameManager.Instance.Player.RemoveItem(Info);
                }
                else
                {
                    GameManager.Instance.Player.SetThrowable(Info as TrowableObject);
                }
            }
        }
    }
}
