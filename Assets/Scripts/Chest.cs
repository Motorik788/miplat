﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SoundEffect))]
public class Chest : MonoBehaviour
{
    public List<InventarCell> ObjectsPrefabs = new List<InventarCell>();
    public float DistanceToTake = 1;
    public bool IsOpen;
    public Sprite IsOpenSprite;
    public UnityEngine.Events.UnityEvent Opened;

    private SoundEffect soundEffect;


    public void TakeAll()
    {
        if (!IsOpen)
        {           
            var player = GameManager.Instance.Player;
            if ((player.transform.position - transform.position).magnitude <= DistanceToTake)
            {
                soundEffect.PlaySound("Open");
                foreach (var item in ObjectsPrefabs)
                {
                    player.AddItem(item.Object, item.Count);
                }
                player.GetComponent<PlayerUI>().ShowChestItems(ObjectsPrefabs);
                SetIsOpenState();
            }
        }
    }

    private void Start()
    {
        SaveManager.Instance.Loaded += Instance_Loaded;
        soundEffect = GetComponent<SoundEffect>();
    }

    public void SetIsOpenState()
    {
        IsOpen = true;
        if (IsOpenSprite)
            GetComponent<SpriteRenderer>().sprite = IsOpenSprite;
        else
            gameObject.SetActive(false);
        Opened.Invoke();
    }

    private void Instance_Loaded()
    {
        if (IsOpen)
        {
            SetIsOpenState();
        }
    }

    private void OnDestroy()
    {
        if (SaveManager.Instance != null)
            SaveManager.Instance.Loaded -= Instance_Loaded;
    }


    //// Update is called once per frame
    //void Update()
    //{

    //}
}
