﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundEffect : MonoBehaviour
{
    [System.Serializable]
    public class Sound
    {
        public string Name;
        public AudioClip[] audioClips;
        public HitInfo.DamageType DamageType;
        [Tooltip("Нужно ли воспроизводить звук только после окончания предыдущего")]
        public bool WhenIsNotPlaying;
        public bool IsOverrideVolume;
        public float OverridedVolume = 1;
        public bool Loop;

        public AudioClip audioClip
        {
            get
            {
                if (audioClips != null && audioClips.Length > 0)
                    return audioClips[Random.Range(0, audioClips.Length - 1)];
                else return null;
            }
        }
        public bool CreateAudioSource;
        public UnityEngine.Audio.AudioMixerSnapshot snapshotEffectTo;
        public UnityEngine.Audio.AudioMixerSnapshot snapshotEffectfrom;
        [HideInInspector]
        public AudioSource source;
    }

    public List<Sound> Sounds;
    [Header("Настройка запуска звука при старте")]
    public bool AutoPlayOnStart;
    public int IndexSoundAutoPlay;

    [HideInInspector]
    public AudioSource Source;

    private void Awake()
    {
        Source = GetComponent<AudioSource>();
        foreach (var item in Sounds)
        {
            if (item.CreateAudioSource)
            {
                item.source = gameObject.AddComponent<AudioSource>();
                item.source.playOnAwake = false;
            }
        }
    }

    void Start()
    {
        if (AutoPlayOnStart && Sounds.Count - 1 >= IndexSoundAutoPlay)
        {
            var sound = Sounds[IndexSoundAutoPlay];
            PlaySound(sound.Name);
        }
    }

    public bool IsPlaying()
    {
        return Source.isPlaying;
    }

    public void Pause()
    {
        Source.Pause();
    }

    public void ContinuePlay()
    {
        PrepareToPlay();
        Source.Play();
    }

    public void PlaySound(string name)
    {
        var sound = Sounds.Find(x => x.Name == name && x.audioClip != null);
        if (sound != null)
        {
            if (atPoint)
            {
                PlaySound(sound.audioClip);
                return;
            }
            PrepareToPlay(sound);
            var source = sound.source != null ? sound.source : Source;
            if (sound.WhenIsNotPlaying && source.isPlaying)//если звук нужно воспроизвести только после окончания предыдущего
                return;
            var clip = sound.audioClip;
            SetClipAndPlayIfCan(source, clip);
            if (!snapCorutine)
            {
                if (sound.snapshotEffectTo)
                {
                    sound.snapshotEffectTo.TransitionTo(3f);
                    if (sound.snapshotEffectfrom)
                        StartCoroutine(SetFromSnapshot(sound.snapshotEffectfrom, clip.length - 2));
                }
            }
        }
    }

    public void PlaySoundHit(HitInfo info)
    {
        var sound = Sounds.Find(x => (x.Name.ToLower() == "hit" || x.Name.ToLower().Contains("hit")) && x.audioClip != null && x.DamageType == info.TypeDamage);
        if (sound != null)
        {
            PrepareToPlay(sound);
            var source = sound.source != null ? sound.source : Source;
            if (sound.WhenIsNotPlaying && source.isPlaying)//если звук нужно воспроизвести только после окончания предыдущего
                return;
            var clip = sound.audioClip;
            SetClipAndPlayIfCan(source, clip);
            if (!snapCorutine)
            {
                if (sound.snapshotEffectTo)
                {
                    sound.snapshotEffectTo.TransitionTo(3f);
                    if (sound.snapshotEffectfrom)
                        StartCoroutine(SetFromSnapshot(sound.snapshotEffectfrom, clip.length - 2));
                }
            }
        }
    }

    bool snapCorutine = false;
    IEnumerator SetFromSnapshot(UnityEngine.Audio.AudioMixerSnapshot snapshotEffectfrom, float t)
    {
        snapCorutine = true;
        yield return new WaitForSeconds(t);
        snapshotEffectfrom.TransitionTo(2f);
        snapCorutine = false;
    }

    public bool atPoint;

    public void PlaySound(AudioClip clip, bool _atPoint = false)
    {
        if (atPoint || _atPoint)
        {
            var g = new GameObject();
            g.transform.position = transform.position;
            var source = g.AddComponent<AudioSource>();
            SetClipAndPlayIfCan(source, clip);
            GameObject.Destroy(g, clip.length + 1);
            return;
        }
        PrepareToPlay();
        SetClipAndPlayIfCan(Source, clip);
    }

    public void PlaySound(AudioClip clip)
    {
        PlaySound(clip, false);
    }

    public void AddClip(string nameClip, AudioClip audio)
    {
        Sounds.Add(new Sound() { audioClips = new AudioClip[] { audio }, Name = nameClip });
    }

    private void PrepareToPlay(Sound sound = null)
    {
        var source = sound != null && sound.source != null ? sound.source : Source;
        source.volume = sound != null && sound.IsOverrideVolume ? sound.OverridedVolume : GameSettingsSingleton.Instance.SoundSettings.EffectVolume;
        source.loop = sound != null ? sound.Loop : false;
    }

    private void SetClipAndPlayIfCan(AudioSource source, AudioClip clip)
    {
        source.clip = clip;
        if (source.clip != null)
            source.Play();
    }
}
