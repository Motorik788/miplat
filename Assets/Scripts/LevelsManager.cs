﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEngine.Events;


[System.AttributeUsage(System.AttributeTargets.Field)]
public class SceneDrawerAttribute : PropertyAttribute { }

#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(SceneDrawerAttribute))]
public class SceneDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var scenes = UnityEditor.EditorBuildSettingsScene.GetActiveSceneList(EditorBuildSettings.scenes);
        for (int i = 0; i < scenes.Length; i++)
            scenes[i] = System.IO.Path.GetFileNameWithoutExtension(scenes[i]);

        property.intValue = EditorGUI.Popup(position, property.name, property.intValue, scenes);
    }
}

#endif

public class LevelsManager : MonoBehaviour
{
    [System.Serializable]
    public class LoadEvent : UnityEvent<int> { }

    [SceneDrawer]
    public int CurrentLevel;
    [SceneDrawer]
    public int SceneLoader;
    [SceneDrawer]
    public int FirstLevel;
    [SceneDrawer]
    public int MenuLevel;

    public LoadEvent loaded;

    public AsyncOperation loadOperation;

    public static LevelsManager Instance
    {
        get
        {
            return instance;
        }
    }

    private static LevelsManager instance;

    public void LoadLevel(int index)
    {
        CurrentLevel = index;
        SceneManager.LoadScene(SceneLoader);
        // SceneManager.LoadScene(index);

    }

    public void LoadFirstLevel()
    {
        LoadLevel(FirstLevel);
    }

    public void LoadMenuLevel()
    {
        LoadLevel(MenuLevel);
    }

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this);
    }


    IEnumerator Lo()
    {
        yield return new WaitForSecondsRealtime(0.1f);
        loadOperation = SceneManager.LoadSceneAsync(CurrentLevel);
        loadOperation.allowSceneActivation = false;
        while (loadOperation.progress < 0.9f)
            yield return null;
        loadOperation.allowSceneActivation = true;
    }

    private void Start()
    {
        loaded.AddListener(x =>
        {
            if (x == SceneLoader)
            {
                StartCoroutine(Lo());
            }
        });
        if (SceneManager.GetActiveScene().buildIndex == CurrentLevel)
        {
            loaded.Invoke(CurrentLevel);
        }
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
    }

    private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        loaded.Invoke(arg0.buildIndex);
    }
}
