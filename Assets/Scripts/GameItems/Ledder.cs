﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TriggerAction2D))]
public class Ledder : MonoBehaviour
{

    public void SetPlayerCanUpLedderOn(Unit unit)
    {
        var playerMove = unit.GetComponent<PlayerMove>();
        playerMove.SetOnLedder(true);       
    }

    public void SetPlayerCanUpLedderOff(Unit unit)
    {
        var playerMove = unit.GetComponent<PlayerMove>();
        playerMove.SetOnLedder(false);

    }

    //void Start()
    //{

    //}


    //void Update()
    //{

    //}
}
