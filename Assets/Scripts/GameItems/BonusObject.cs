﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ImmunityBonus : Bonus
{
    public List<Unit.ImmunityDamageLayer> Immunities = new List<Unit.ImmunityDamageLayer>();

    [HideInInspector]
    public List<Unit.ImmunityDamageLayer> immunitiesPrev = new List<Unit.ImmunityDamageLayer>();

    public override void Apply(PlayerUnit unit, PlayerMove playerMove)
    {
        foreach (var item in Immunities)
        {
            Unit.ImmunityDamageLayer layer = new Unit.ImmunityDamageLayer() { DamageType = item.DamageType, Value = unit.GetImmunity(item.DamageType) };
            immunitiesPrev.Add(layer);
            unit.SetImmunity(item.DamageType, item.Value);
        }
    }

    public override void RollBack(PlayerUnit unit, PlayerMove playerMove)
    {
        foreach (var item in immunitiesPrev)
            unit.SetImmunity(item.DamageType, item.Value);
        immunitiesPrev.Clear();
    }

    public ImmunityBonus(ImmunityBonus immunityBonus) : base(immunityBonus)
    {
        Immunities = immunityBonus.Immunities;
        immunitiesPrev = immunityBonus.immunitiesPrev;
    }
    public ImmunityBonus() { }
}

[System.Serializable]
public class Bonus
{
    public enum EffectType
    {
        ChangeMaxHealth,
        ChangeHealth,
        ChangeArmor,
        ChangeMaxArmor,
        ChangeWeapon,
        ChangeSpeed,
        GetImmunityHit,
        GetImmunityArea,
        GetImmunityBonus
    }
    public string Name;
    public bool IsTemp;
    public bool IsIncrement;
    public float IncrementLastTime;
    public float Time;
    public float Value;
    public float PrevValue;
    public EffectType Type;

    public virtual void Apply(PlayerUnit unit, PlayerMove playerMove)
    {
    }

    public virtual void RollBack(PlayerUnit unit, PlayerMove playerMove)
    {

    }

    public Bonus(Bonus bonus)
    {
        IncrementLastTime = bonus.IncrementLastTime;
        IsIncrement = bonus.IsIncrement;
        IsTemp = bonus.IsTemp;
        Time = bonus.Time;
        Value = bonus.Value;
        PrevValue = bonus.PrevValue;
        Type = bonus.Type;
        Name = bonus.Name;
    }

    public Bonus() { }
}


public class BonusObject : ObjectInfo
{
    public List<Bonus> Bonuses = new List<Bonus>();


    public virtual string GetBonusDescription()
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.AppendLine("Эффекты:");
        foreach (var item in Bonuses)
        {
            sb.AppendFormat("{0} - {1} " + (item.IsTemp ? "(временный) " + item.Time + "c" : "(постоянный)") + "\n", item.Type, item.Value);
        }
        return sb.ToString();
    }

    public override string ToString()
    {
        return Description + "\n" + GetBonusDescription();
    }

    public override void Use()
    {
        base.Use();
        foreach (var item in Bonuses)
        {
            BonusEffectSystem.Instance.AddBonus(item);
        }
    }
}
