﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrowableObject : ObjectInfo
{
    public enum TypeTrowable
    {
        Trap,
        Bomb,
        Knife
    }

    public TrowableObject()
    {
        Type = ObjectType.Trowable;
    }

    public float Damage;
    public float ThrowForceMultiply = 1;
    public TypeTrowable TrowableType;
    public bool IsUse;

    private Unit from;

    public void SetFromUnit(Unit unit)
    {
        from = unit;
    }

    public Unit GetFromUnit()
    {
        return from;
    }

    public override void Use()
    {
        IsUse = true;
    }
}
