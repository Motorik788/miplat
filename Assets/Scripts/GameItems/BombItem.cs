﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(TrowableObject))]
public class BombItem : MonoBehaviour
{
    public float Delay;
    public float Radius;
    public float PhysicForce;
    public GameObject ExploEffect;
    public float TimeToCollectEffect = 1.5f;
    [HideInInspector]
    public bool Activate;
    public UnityEvent ExploEvent;


    private TrowableObject TrowableObject;


    void Start()
    {
        TrowableObject = GetComponent<TrowableObject>();
        if (TrowableObject.TrowableType == TrowableObject.TypeTrowable.Bomb)
            Activate = true;
        else
            TrowableObject.IsUse = false;
    }


    void Update()
    {
        if (Activate)
        {
            var use = TrowableObject.IsUse;
            if (use)
            {
                Delay -= Time.deltaTime;
                if (Delay <= 0)
                {
                    Explosion();
                }
            }
        }
        else
        {
            var coll = GetComponent<Collider2D>();
            var posRay = new Vector2(coll.bounds.center.x, coll.bounds.center.y);
            posRay.y = GetComponent<Collider2D>().bounds.min.y - 0.02f;
            var hit = Physics2D.Raycast(posRay, -transform.up, 0.05f);
            if (hit.collider)
            {
                Activate = true;
            }
        }
    }

    public void PrepareBombExpllo()
    {
        TrowableObject.IsUse = true;
    }

    public void Explosion()
    {
        TrowableObject.IsUse = false;
        var colliders = Physics2D.OverlapCircleAll(transform.position, Radius);
        foreach (var item in colliders)
        {
            var dir = (item.transform.position - transform.position);
            float wearoff = 1 - (dir.magnitude / Radius);
            var unit = item.GetComponent<Unit>();
            var bomb = item.GetComponent<BombItem>();
            if (bomb && bomb.gameObject != gameObject && bomb.enabled)
            {
                bomb.Delay = 0.3f;
                bomb.TrowableObject.IsUse = true;
                bomb.Activate = true;
                continue;
            }
            if (unit)
                unit.Damage(new HitInfo()
                {
                    Value = TrowableObject.Damage * wearoff,
                    TypeDamage = HitInfo.DamageType.Explosion,
                    MaxValue = TrowableObject.Damage,
                    From = TrowableObject.GetFromUnit()
                });
            else if (item.attachedRigidbody && !item.isTrigger)
            {
                item.attachedRigidbody.AddForce(dir.normalized * PhysicForce * wearoff * 10);
            }
        }
        if (ExploEffect)
        {
            var obj = ObjectPool.Instance.Create(ExploEffect, transform.position, Quaternion.identity);
            ObjectPool.Instance.Collect(obj, TimeToCollectEffect);
        }
        ExploEvent.Invoke();
        Destroy(gameObject, 0.2f);
    }
}
