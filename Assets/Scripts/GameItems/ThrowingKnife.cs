﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TriggerAction2D))]
[RequireComponent(typeof(SoundEffect))]
public class ThrowingKnife : TrowableObject
{
    public float Speed;
    public float SpeedRotate;
    public SoundEffect SoundEffect;
    [HideInInspector]
    public Vector3 dir;

    public ThrowingKnife()
    {
        TrowableType = TypeTrowable.Knife;
    }


    void Start()
    {
        var unit = GameManager.Instance.Player;
        SoundEffect = GetComponent<SoundEffect>();
        dir = unit.transform.right;
        SpeedRotate = unit.lookDir == PlayerUnit.LookDir.Right ? SpeedRotate * -1 : SpeedRotate;    
        SoundEffect.PlaySound("Swing");
    }


    void Update()
    {
        transform.Rotate(new Vector3(0, 0, SpeedRotate * Time.deltaTime * 10));
        transform.position += dir * Speed * Time.deltaTime;
    }

    public void OnHit(Unit unit)
    {
        unit.Damage(new HitInfo() { From = GameManager.Instance.Player, Value = Damage, TypeDamage = HitInfo.DamageType.Projectile });
        Destroy(gameObject);
    }
}
