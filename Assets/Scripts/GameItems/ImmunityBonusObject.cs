﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImmunityBonusObject : BonusObject
{
    public ImmunityBonus ImmunityBonus;

    public override void Use()
    {
        base.Use();
        if (ImmunityBonus.Immunities.Count > 0)
            BonusEffectSystem.Instance.AddBonus(ImmunityBonus);
    }
}
