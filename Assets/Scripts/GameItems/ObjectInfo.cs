﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInfo : MonoBehaviour
{
    public enum ObjectType
    {
        Book,
        Potion,
        Trowable,
        Other,
        Quest,
        Eat
    }

    public string Name;
    public ObjectType Type;
    public Sprite SpriteIcon;
    public AudioClip soundUse;
    [TextArea]
    public string Description;

    public override string ToString()
    {
        return Description;
    }

    public virtual void Use()
    {
        if (soundUse)
            GameManager.Instance.Player.SoundEffect.PlaySound(soundUse, true);
    }
}
