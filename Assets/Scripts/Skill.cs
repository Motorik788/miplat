﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(ImmunityBonusObject))]
public class Skill : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public bool IsActive = true;
    public bool IsOpen = true;
    public int PriceXp;
    public Skill Depend;
    public SkilsTree SkilsTree;

    [HideInInspector]
    public BonusObject bonusObject;

    private Outline outline;
    private ToolTipObject toolTip;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (IsActive && IsOpen)
        {
            IsActive = !SkilsTree.Activate(this, PriceXp);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (IsActive && IsOpen)
        {
            outline.enabled = true;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (IsActive && IsOpen)
        {
            outline.enabled = false;
        }
    }

    public void Open(bool open = true)
    {
        if (IsActive)
        {
            IsOpen = open;
            var image = GetComponent<Image>();
            var color = image.color;
            color.a = open ? 1 : .4f;
            image.color = color;
        }
    }

    void Start()
    {
        outline = GetComponent<Outline>();
        toolTip = GetComponent<ToolTipObject>();
        toolTip.Text += ". (" + PriceXp + "xp)";
        bonusObject = GetComponent<BonusObject>();
        if (Depend && Depend.IsActive)
        {
            Open(false);
        }
    }
}
