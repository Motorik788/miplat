﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkilsTree : MonoBehaviour, ISave
{
    public BonusObject[] Skills;
    public ShowStatsUILevel ShowStatsUI;

    [SerializeField]
    private bool[] activeSkills;

    public bool Activate(Skill skill, int price)
    {
        if (GameManager.Instance.XP >= price)
        {
            for (int i = 0; i < Skills.Length; i++)
            {
                if (Skills[i] == skill.bonusObject)
                {
                    activeSkills[i] = true;
                    break;
                }
            }
            for (int i = 0; i < Skills.Length; i++)
            {
                var _skill = Skills[i].GetComponent<Skill>();
                if (_skill.Depend == skill)
                {
                    _skill.Open();
                }
            }
            skill.bonusObject.Use();
            GameManager.Instance.XP -= price;
            if (ShowStatsUI)
                ShowStatsUI.UpdateTextAllXp();
            return true;
        }
        return false;
    }


    public string GetId()
    {
        return "SkillsTree";
    }

    public bool IsGlobal()
    {
        return true;
    }

    public void Load(Save save)
    {
        var val = save.Components[0].sps[0].Val;
        if (val != null)
            activeSkills = (bool[])val;
        for (int i = 0; i < activeSkills.Length; i++)
        {
            if (activeSkills[i])
            {
                Skills[i].gameObject.GetComponent<UnityEngine.UI.Outline>().enabled = true;
                var skill = Skills[i].gameObject.GetComponent<Skill>();
                skill.IsActive = false;           
            }
        }
        for (int i = 0; i < Skills.Length; i++)
        {
            var skill = Skills[i].GetComponent<Skill>();
            if (skill.Depend && !skill.Depend.IsActive)
            {
                skill.Open();
            }
        }
    }

    public Save Save()
    {
        Save s = new Save();
        SComp comp = new SComp();
        comp.Component = "skills";
        comp.sps.Add(new SProp() { Prop = "activeSkills", Val = activeSkills });
        s.Components.Add(comp);
        return s;
    }

    private void OnDestroy()
    {
        SaveManager.UnRegister(this);
    }


    void Start()
    {
        SaveManager.Register(this);
        activeSkills = new bool[Skills.Length];
    }


    void Update()
    {

    }
}
