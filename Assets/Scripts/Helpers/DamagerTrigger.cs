﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagerTrigger : TriggerAction2D
{
    [Header("Damager Settings")]
    public float Value;
    public HitInfo.DamageType DamageType;
    public GameObject BloodDecalPrefab;
    public float DecalCenterOffset = 1.5f;


    public void ApplyDamage(Unit unit)
    {
        unit.Damage(new HitInfo() { From = null, Value = Value, TypeDamage = DamageType });
    }

    public void TeleP(GameObject go)
    {
        go.transform.position = go.transform.position + transform.up * 5;
    }

    public void DrawBloodDecal(Unit unit)
    {
        if (BloodDecalPrefab)
        {
            var blood = Instantiate(BloodDecalPrefab, col.bounds.center, Quaternion.identity);
            blood.transform.position = Vector3.MoveTowards(blood.transform.position, unit.transform.position, Random.Range(DecalCenterOffset / 2.5f, DecalCenterOffset));
            blood.transform.Rotate(new Vector3(0, 0, Random.Range(0, 280)));
            blood.transform.SetParent(transform);
            blood.transform.localPosition = new Vector3(blood.transform.localPosition.x, blood.transform.localPosition.y, 0);
            blood.transform.localPosition += new Vector3(0, 0, -0.1f);

        }
    }

}
