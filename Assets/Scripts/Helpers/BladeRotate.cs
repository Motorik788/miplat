﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BladeRotate : MonoBehaviour
{
    public float Speed;

    public float angle;

    public float a;
    public bool IsSimpleRotate;

    bool b = false;

    // float sav;
    private void Start()
    {
        // sav = transform.eulerAngles.z;
        angle = transform.eulerAngles.z;
        if (angle > 97)
            angle = -(360 - angle);
    }

    void Update()
    {
        if (!IsSimpleRotate)
        {
            a = Mathf.InverseLerp(97, 35, Mathf.Abs(angle));
            if (angle + 0.1f > 90f)
                b = true;
            if (angle - 0.1f < -90)
                b = false;
            if (!b)
                angle += Time.deltaTime * Speed * a;
            else
                angle -= Time.deltaTime * Speed * a;
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, angle);
        }
        else
        {
            transform.Rotate(new Vector3(0, 0, Speed * Time.deltaTime));
        }
    }
}
