﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMoveTo : MonoBehaviour
{
    public enum Dir
    {
        X,
        Y
    }

    public Dir Direction = Dir.Y;
    public float Speed = 1;
    public float Lenght;
    public bool AutoLoop;
    public float Wait;
    public bool Active = true;
    public bool UseLocal = true;

    private Vector3 startPos;
    private bool isStart = false;
    private bool isEnd = false;
    private Vector3 nPos;
    private float lastWaitTime;

    public void Move()
    {
        if (!isStart)
        {
            isStart = true;
            isEnd = false;
            nPos = startPos + (Direction == Dir.Y ? (UseLocal ? transform.up : Vector3.up) : (UseLocal ? transform.right : Vector3.right)) * Lenght;
        }
    }

    public void MoveLoop()
    {
        enabled = true;
        AutoLoop = true;
        Move();
    }

    public void MoveLoop(float startDelay)
    {
        enabled = true;
        AutoLoop = true;
        Move();
        lastWaitTime = Time.time + startDelay;
    }

    public void ReturnMove()
    {
        isEnd = true;
        isStart = false;
    }

    private void Start()
    {
        startPos = transform.position;
        if (AutoLoop)
        {
            Move();
        }
    }

    void Update()
    {
        if (Active && Time.time - lastWaitTime >= Wait)
        {
            if (isStart)
            {
                transform.position = Vector3.MoveTowards(transform.position, nPos, Time.deltaTime * Speed);
                if (Vector3.Distance(transform.position, nPos) <= 0.01f)
                {
                    isStart = false;
                    isEnd = AutoLoop ? true : false;
                    lastWaitTime = Time.time;
                }
            }
            if (isEnd)
            {
                transform.position = Vector3.MoveTowards(transform.position, startPos, Time.deltaTime * Speed);
                if (Vector3.Distance(transform.position, startPos) <= 0.01f)
                {
                    isEnd = false;
                    isStart = AutoLoop ? true : false;
                    lastWaitTime = Time.time;
                }
            }
        }
    }
}
