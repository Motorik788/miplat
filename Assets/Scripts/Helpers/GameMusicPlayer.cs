﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class LevelMusic
{
    [SceneDrawer]
    public int Level;
    public AudioClip audioClip;
}

public class GameMusicPlayer : MonoBehaviour
{
    public AudioSource source;
    public List<LevelMusic> levelMusics = new List<LevelMusic>();

    private bool isTransition = false;
    private bool isUpVolume = false;
    private LevelMusic cur;

    public void LoadLVL(int index)
    {
        var lvlMus = levelMusics.Find(x => x.Level == index);
        if (lvlMus != null)
        {
            cur = lvlMus;
            if (!source.clip || (source.clip && source.clip != cur.audioClip))
                isTransition = true;
        }
    }


    public void PlayMusic(LevelMusic levelMusic)
    {
        source.clip = levelMusic.audioClip;
        source.Play();
    }
    //void Start()
    //{

    //}


    void Update()
    {
        if (isTransition)
        {
            if (!isUpVolume)
            {
                if (source.volume >= 0.1f)
                    source.volume -= Time.unscaledDeltaTime / 1.6f;
                else if (!isUpVolume)
                {
                    isUpVolume = true;
                    PlayMusic(cur);
                }
            }
            else
            {
                source.volume += Time.unscaledDeltaTime / 1.6f;
                if (source.volume >= GameSettingsSingleton.Instance.SoundSettings.MusicVolume)
                {
                    source.volume = GameSettingsSingleton.Instance.SoundSettings.MusicVolume;
                    isUpVolume = false;
                    isTransition = false;
                }
            }
        }
    }
}
