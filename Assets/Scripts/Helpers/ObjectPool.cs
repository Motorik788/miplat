﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface IPoolObject
{
    void OnCollect();
    void OnSpawn();
}

public class ObjectPool : MonoBehaviour
{
    public static ObjectPool Instance { get; private set; }

    public List<GameObject> Pool = new List<GameObject>();

    public void Collect(GameObject go)
    {
        IPoolObject poolObject = go.GetComponent<IPoolObject>();
        if (poolObject != null)
            poolObject.OnCollect();
        go.SetActive(false);
        go.transform.SetParent(transform);
        Pool.Add(go);
    }

    public void Collect(GameObject go, float t)
    {
        StartCoroutine(CollectDelay(go, t));
    }

    private IEnumerator CollectDelay(GameObject go, float t)
    {
        yield return new WaitForSeconds(t);
        Collect(go);
    }

    public GameObject Create(GameObject go, Vector3 pos, Quaternion rot)
    {
        var obj = Pool.Find((c) => c.name == go.name);
        if (obj)
        {
            obj.SetActive(true);
            obj.transform.SetParent(null);
            obj.transform.position = pos;
            obj.transform.rotation = rot;
            Pool.Remove(obj);
        }
        else
        {
            obj = GameObject.Instantiate(go, pos, rot);
            obj.name = go.name;
        }
        IPoolObject poolObject = obj.GetComponent<IPoolObject>();
        if (poolObject != null)
            poolObject.OnSpawn();
        return obj;
    }

    public T Create<T>(T go) where T : MonoBehaviour
    {
        return Create(go.gameObject, go.transform.position, go.transform.rotation).GetComponent<T>();
    }

    public GameObject Create(GameObject go)
    {
        return Create(go, go.transform.position, go.transform.rotation);
    }

    void Awake()
    {
        if (Instance == null)
            Instance = this;
    }


    //void Update()
    //{

    //}
}
