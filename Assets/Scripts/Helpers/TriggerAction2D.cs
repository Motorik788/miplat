﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class TagPropertyAttribute : PropertyAttribute { }

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(TagPropertyAttribute))]
public class TagProperty : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        property.stringValue = EditorGUI.TagField(position, label, property.stringValue);
        if (!string.IsNullOrEmpty(property.stringValue) && property.stringValue == "Untagged")
            property.stringValue = null;
    }
}

#endif


[System.Serializable]
public class TriggerUnitEvent : UnityEngine.Events.UnityEvent<Unit> { }

[System.Serializable]
public class TriggerGOEvent : UnityEngine.Events.UnityEvent<GameObject> { }

public enum TriggerEventType
{
    Exit,
    Stay,
    StayRepeater,
    Enter
}

public enum Target
{
    OnlyUnit,
    All
}

public enum TriggerType
{
    Trigger,
    Collision
}

[System.Serializable]
public class ActionTrigger
{
    public bool IsActive = true;
    public TriggerEventType EventType;
    public Target TargetType;
    [Tooltip("Must be empty to any tag")]
    [TagProperty]
    public string TargetTag;
    [Header("Для всех (Nothing, Everything)")]
    public LayerMask TargetLayer = 0;
    public bool Once;
    public bool UseTimer;
    public float Timer;
    [Header("Вызываются, если TargetType = OnlyUnit")]
    public TriggerUnitEvent HandlersTargetUnit;
    [Header("Вызываются, если TargetType = All")]
    public TriggerGOEvent HandlersTargetAll;

    float timerSave;
    bool activate = false;
    Unit unit = null;
    GameObject Go;

    public void Start()
    {
        timerSave = Timer;
    }

    public void Invoke(bool immediate = false)
    {
        if (!UseTimer || immediate)
        {
            if (TargetType == Target.All)
            {
                HandlersTargetAll.Invoke(Go);
            }
            else
            {
                HandlersTargetUnit.Invoke(unit);
            }
            IsActive = Once ? false : true;
        }
        else if (EventType == TriggerEventType.Stay || EventType == TriggerEventType.StayRepeater)
        {

            Timer = Timer > 0 ? Timer - Time.deltaTime : Timer;
            if (Timer <= 0)
            {
                Invoke(true);
                if (EventType == TriggerEventType.StayRepeater)
                    Timer = timerSave;
            }

        }
    }

    public bool TestCollider(GameObject go, TriggerEventType type)
    {
        var valInMask = LayerMask.GetMask(LayerMask.LayerToName(go.layer));
        if ((IsActive || activate) && (((TargetLayer.value | valInMask) == TargetLayer.value) || TargetLayer.value == 0))
        {
            if (TargetType == Target.OnlyUnit)
                unit = go.GetComponent<Unit>();
            else
                Go = go;
            if (unit || TargetType == Target.All)
            {
                if (!string.IsNullOrEmpty(TargetTag))
                    return go.tag == TargetTag;

                if (UseTimer)
                    activate = true;
                return true;

            }
        }
        return false;
    }

    public void UpdateTimer()
    {
        if ((IsActive || Once) && UseTimer && activate)
        {
            if (EventType != TriggerEventType.Stay && EventType != TriggerEventType.StayRepeater)
            {
                Timer = Timer > 0 ? Timer - Time.deltaTime : Timer;
                if (Timer <= 0)
                {
                    Invoke(true);
                    activate = false;
                    Timer = timerSave;
                }
            }
        }
    }

    public void ExitTimerForStay()
    {
        if (UseTimer && (EventType == TriggerEventType.Stay || EventType == TriggerEventType.StayRepeater))
        {
            activate = false;
            Timer = timerSave;
        }
    }
}

public class TriggerAction2D : MonoBehaviour
{
    public bool IsActive = true;
    public TriggerType Type;
    public ActionTrigger[] Triggers = new ActionTrigger[1];

    protected Collider2D col;
    System.Collections.Generic.Stack<ActionTrigger> curs = new Stack<ActionTrigger>();


    void Start()
    {
        foreach (var item in Triggers)
            item.Start();
        col = GetComponent<Collider2D>();
        if (!col)
            Debug.LogErrorFormat("Нет коллайдера на объекте триггера '{0}'", name);
        if (Type == TriggerType.Trigger)
            col.isTrigger = true;
    }

    private void Invoke()
    {
        while (curs.Count > 0)
            curs.Pop().Invoke();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (Type == TriggerType.Trigger && enabled)
            Enter(collision.gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (Type == TriggerType.Collision && enabled)
            Enter(collision.gameObject);
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (Type == TriggerType.Collision && enabled)
            Stay(collision.gameObject);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (Type == TriggerType.Collision && enabled)
            Exit(collision.gameObject);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (Type == TriggerType.Trigger && enabled)
            Exit(collision.gameObject);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (Type == TriggerType.Trigger && enabled)
            Stay(collision.gameObject);
    }

    private void Enter(GameObject go)
    {
        if (TestCollider(go, TriggerEventType.Enter))
            Invoke();
    }

    private void Exit(GameObject go)
    {
        if (TestCollider(go, TriggerEventType.Exit))
            Invoke();
        foreach (var item in Triggers)
            item.ExitTimerForStay();
    }

    private void Stay(GameObject go)
    {
        if (TestCollider(go, TriggerEventType.Stay) | TestCollider(go, TriggerEventType.StayRepeater))
            Invoke();
    }

    private bool TestCollider(GameObject go, TriggerEventType type)
    {
        if (IsActive && enabled)
        {
            foreach (var item in Triggers)
            {
                if (item.EventType == type && item.TestCollider(go, type))
                    curs.Push(item);
            }
            return true;
        }
        return false;
    }



    protected void Update()
    {
        if (IsActive)
        {
            foreach (var item in Triggers)
                item.UpdateTimer();
        }
    }
}
