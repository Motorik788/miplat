﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoaderLevel : MonoBehaviour
{
    [SceneDrawer]
    public int To;


    public void Load()
    {
        // LevelsManager.Instance.LoadLevel(To);
        GameManager.Instance.Player.GetComponent<PlayerUI>().PrepareLoadLevel(To);
    }
}
