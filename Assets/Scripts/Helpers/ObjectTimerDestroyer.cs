﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectTimerDestroyer : MonoBehaviour
{
    public float Delay;
    public bool UsePoolObject;


    void Update()
    {
        Delay -= Time.deltaTime;
        if (Delay < 0)
        {
            if (!UsePoolObject)
                Destroy(gameObject);
            else
                ObjectPool.Instance.Collect(gameObject);
        }

    }

    public void SetDelay(float delay)
    {
        Delay = delay;
    }
}
