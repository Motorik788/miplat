﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PoolObjectEventHandler : MonoBehaviour, IPoolObject
{
    [System.Serializable]
    public class IPoolObjectEvent : UnityEvent { }

    public IPoolObjectEvent OnCollectHandlers;
    public IPoolObjectEvent OnSpawnHandlers;



    public void OnCollect()
    {
        OnCollectHandlers.Invoke();
    }

    public void OnSpawn()
    {
        OnSpawnHandlers.Invoke();
    }
}
