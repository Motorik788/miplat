﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureOffsetAnimate : MonoBehaviour
{
    public float speed;

    private new Renderer renderer;
    void Start()
    {
        renderer = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        var curOffset = renderer.material.GetTextureOffset("_MainTex");
        renderer.material.SetTextureOffset("_MainTex", new Vector2(curOffset.x + speed * Time.deltaTime, 0));
    }
}
