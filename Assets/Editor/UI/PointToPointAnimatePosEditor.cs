﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PointToPointAnimatePos))]
public class PointToPointAnimatePosEditor : Editor
{
    
    public override void OnInspectorGUI()
    {
        var obj = target as PointToPointAnimatePos;

        EditorGUILayout.BeginVertical();
        
        EditorGUILayout.LabelField("Анимирует текущий обьект перемещая из точки в точку.", GUILayout.Height(30));
        EditorGUILayout.BeginHorizontal();
        obj.StartPos = EditorGUILayout.Vector3Field("Start Position", obj.StartPos);
        if (GUILayout.Button("Current pos",GUILayout.Width(80)))
            obj.StartPos = obj.transform.position;
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        obj.EndPos = EditorGUILayout.Vector3Field("End Position", obj.EndPos);
        if (GUILayout.Button("Current pos", GUILayout.Width(80)))
            obj.EndPos = obj.transform.position;
        EditorGUILayout.EndHorizontal();
        obj.Speed = EditorGUILayout.FloatField("Speed Animation",obj.Speed);
        EditorGUILayout.EndVertical();
    }
}
