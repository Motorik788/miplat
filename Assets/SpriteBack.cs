﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteBack : MonoBehaviour
{
    public Transform target;
    public float speed = 1;
    public bool LockY;

    Vector3 o;
    void Start()
    {
        o = transform.position - target.position;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var pos = target.position + o;
        if (LockY)
            pos.y = transform.position.y;
        var horizontal = InputManager.Instance.GetActionAxis("walk", true);
        if (horizontal != 0)
        {

            o -= transform.right * speed * Time.deltaTime * horizontal;
        }

        transform.position = pos;
    }
}
